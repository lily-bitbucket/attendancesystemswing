This directory contains the source code for CS6359-002 Assignment4.

Compilation and run
-------------------
There are two ways to compile and run this program:

1.  Use javac. In this folder, issue command:

    javac lili/AttendanceApp.java
    java  lili/AttendanceApp

2.  Use ant. In this folder, issue command:
    
    ant build
    java lili/AttendanceApp


Development
-----------
This program is developed by Shuangli Wu, Net ID: sxw133730.
