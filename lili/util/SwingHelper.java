package lili.util;

import javax.swing.JOptionPane;

/**
 * This class contains all the helper functions for validating data as well as
 * displaying various error messages for the UI modules.
 * 
 * @author Shuangli Wu
 * @NetID sxw133730
 * @date 03/25/2014
 * @purpose CS6359-002 Assignment4
 */
public class SwingHelper {

	/**
	 * Validate that a string is not null or empty
	 * 
	 * @param s
	 *            the string to validate
	 * @return whether the string is empty
	 */
	public static boolean validateNotEmpty(String s) {
		return s != null && !s.trim().equals("");
	}

	/**
	 * Validate that a string is not empty and is numeric
	 * 
	 * @param s
	 *            the string to validate
	 * @return true if the string is non-empty and numeric
	 */
	public static boolean validateNotEmptyNumeric(String s) {
		if (validateNotEmpty(s)) {
			try {
				Integer.parseInt(s);
				return true;
			} catch (Exception e) {
				return false;
			}
		} else
			return false;
	}

	/**
	 * Display a message box.
	 * 
	 * @param msg
	 *            the given message
	 */
	public static void messageBox(String msg) {
		JOptionPane.showMessageDialog(null, msg);
	}

	/**
	 * Display the error message for invalid user input format.
	 */
	public static void invalidInputFormat() {
		invalidInputFormat("");
	}

	/**
	 * Display the error message for invalid user input format.
	 * 
	 * @param extraInfo
	 *            extra detailed information to display
	 */
	public static void invalidInputFormat(String extraInfo) {
		if (extraInfo != "")
			extraInfo = " (" + extraInfo + ")";
		messageBox("Input format is invalid. Please check your input"
				+ extraInfo + ".");
	}

	/**
	 * Display the error message for a request that cannot be processed
	 */
	public static void badRequest() {
		badRequest("");
	}

	/**
	 * Display the error message for a request that cannot be processed
	 * 
	 * @param extraInfo
	 *            extra detailed information to display
	 */
	public static void badRequest(String extraInfo) {
		if (extraInfo != "")
			extraInfo = " (" + extraInfo + ")";
		messageBox("Request cannot be processd or accepted. Please check your input"
				+ extraInfo + ".");
	}

	/**
	 * Display the message for a successful save operation.
	 */
	public static void saveSuccess() {
		messageBox("Data saved successfully.");
	}

	/**
	 * Display the message for a successful delete operation.
	 */
	public static void deleteSuccess() {
		messageBox("Data deleted successfully.");
	}

	/**
	 * Display the message for a successful update operation.
	 */
	public static void updateSuccess() {
		messageBox("Data updated successfully.");
	}
	
	/**
	 * Display the message for a successful insert/add operation.
	 */
	public static void addSuccess() {
		messageBox("Data added successfully.");
	}

	/**
	 * Display the error message for not selecing a row in a table
	 */
	public static void noRowSelected() {
		messageBox("Please select one line first.");
	}

	/**
	 * Display the error message for not changing the table and click modify
	 */
	public static void noChanges() {
		messageBox("No data has been changed.");
	}
}
