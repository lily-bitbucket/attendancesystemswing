package lili.service;

import java.util.Iterator;
import java.util.List;

import javax.swing.table.DefaultTableModel;

import lili.dao.CourseDao;
import lili.dao.EnrollmentDao;
import lili.dao.StudentDao;

/**
 * This class constructs varioius table models from course data, student data
 * and enrollment data.
 * 
 * @author Shuangli Wu
 * @NetID sxw133730
 * @date 03/25/2014
 * @purpose CS6359-002 Assignment4
 */
public class TableModelService extends BaseService {

	/** Data access objects needed */
	CourseDao courseDao = new CourseDao();
	StudentDao studentDao = new StudentDao();
	EnrollmentDao enrollmentDao = new EnrollmentDao();

	/**
	 * Generate a table model with course data
	 * 
	 * @return course data table model
	 */
	public DefaultTableModel getCourseTableModel() {
		DefaultTableModel courseModel = new DefaultTableModel(
				courseDao.loadCoursesData(), new String[] { "CourseID",
						"CourseName" });
		return courseModel;
	}

	/**
	 * Generate a table model with student data
	 * 
	 * @return student data table model
	 */
	public DefaultTableModel getStudentTableModel() {
		DefaultTableModel studentModel = new DefaultTableModel(
				studentDao.loadStudentsData(), new String[] { "StudentID",
						"FirstName", "LastName", "Avatar" });

		return studentModel;
	}

	/**
	 * Generate a table model with enrollment data of a specific course
	 * 
	 * @param cid
	 *            given courseID
	 * @return enrollment data table model of the given course
	 */
	public DefaultTableModel getEnrollmentTableModelByCourseId(String cid) {
		List<String[]> list = enrollmentDao.loadEnrollmentsDataList();
		Iterator<String[]> iter = list.iterator();
		while (iter.hasNext()) {
			if (!iter.next()[1].equals(cid))
				iter.remove();
		}
		if (list.size() > 0) {
			String[][] list2D = new String[list.size()][list.get(0).length];
			for (int i = 0; i < list2D.length; i++) {
				list2D[i] = list.get(i);
			}
			return new DefaultTableModel(list2D, new String[] { "StudentID",
					"CourseID" });
		} else {
			return new DefaultTableModel(null, new String[] { "StudentID",
					"CourseID" });
		}
	}

	/**
	 * Generate a table model with enrollment data
	 * 
	 * @return generated enrollment model
	 */
	public DefaultTableModel getEnrollmentTableModel() {
		DefaultTableModel enrollmentModel = new DefaultTableModel(
				enrollmentDao.loadEnrollmentsData(), new String[] {
						"StudentID", "CourseID" });
		return enrollmentModel;
	}
}
