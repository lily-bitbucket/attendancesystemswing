package lili.service.io;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * This class is the lower level input and ouput management. It handles reading
 * and writing CSV files. It is implemented as singleton because it will be
 * shared by all DAOs.
 * 
 * @author Shuangli Wu
 * @NetID sxw133730
 * @date 03/25/2014
 * @purpose CS6359-002 Assignment4
 */
public class DataManager {

	/** Reader/writer objects handling line-based file access. */
	BufferedReader reader;
	BufferedWriter writer;

	/** Singleton */
	public static DataManager inst = null;

	/** Method that initializes or returns the singleton */
	public static DataManager getUniqueInstance() {
		if (inst == null)
			inst = new DataManager();
		return inst;
	}

	/** Hiding the default constructor */
	private DataManager() {
	}

	/**
	 * Read CSV files into a list of String[] objects
	 * 
	 * @param csvFilePath
	 *            the path to the CSV file to read
	 * @return the content of the CSV file
	 */
	public List<String[]> readCSV(String csvFilePath) {
		List<String[]> result = new ArrayList<String[]>();
		try {
			File file = new File(csvFilePath);
			if (!file.exists()) {
				file.getParentFile().mkdirs();
				file.createNewFile();
			}
			reader = new BufferedReader(new FileReader(file));
			String line = null;
			while ((line = reader.readLine()) != null) {
				String[] tokens = line.split(",");
				result.add(tokens);
			}
			reader.close();
		} catch (Exception e) {
			System.out.println("Unable to read file with path " + csvFilePath
					+ ", and unable to create an empty file.");
		}
		return result;
	}

	/**
	 * Read CSV files into aString[][] object, which is convenient for
	 * populating table models
	 * 
	 * @param csvFilePath
	 *            the path to the CSV file to read
	 * @return the content of the CSV file
	 */
	public String[][] readCSV2DArray(String csvFilePath) {
		List<String[]> result = readCSV(csvFilePath);
		String[][] result2D = new String[result.size()][result.get(0).length];
		for (int row = 0; row < result2D.length; row++) {
			result2D[row] = result.get(row);
		}
		return result2D;
	}

	/**
	 * Write the given data to the CSV file. It overwrites all the existing
	 * content for simplicity.
	 * 
	 * @param csvFilePath
	 *            the path to the CSV file to write
	 * @param data
	 *            the data to write
	 */
	public void writeCSV(String csvFilePath, List<String[]> data) {
		try {
			File file = new File(csvFilePath);
			if (!file.exists()) {
				file.getParentFile().mkdirs();
				file.createNewFile();
			}
			writer = new BufferedWriter(new FileWriter(file));
			for (String[] entry : data) {
				String line = "";
				for (int anEntry = 0; anEntry < entry.length; anEntry++)
					if (anEntry == entry.length - 1)
						line += entry[anEntry];
					else
						line += entry[anEntry] + ",";
				line += "\n";
				writer.write(line);
			}
			writer.close();
		} catch (IOException e) {
			System.out.println("Unable to write to file with path "
					+ csvFilePath + ", and unable to create an empty file.");
		}
	}

	/**
	 * This method handles appending data to an existing CSV file
	 * 
	 * @param csvFilePath
	 *            the path to the CSV file to write
	 * @param data
	 *            the data to append
	 */
	public void appendToCSV(String csvFilePath, String[] data) {
		List<String[]> csvData = readCSV(csvFilePath);
		csvData.add(data);
		writeCSV(csvFilePath, csvData);
	}
}
