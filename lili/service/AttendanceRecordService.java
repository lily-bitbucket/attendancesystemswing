package lili.service;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import lili.Config;
import lili.dao.AttendanceRecordDao;
import lili.dao.CourseDao;
import lili.domainmodel.AttendanceRecord;
import lili.domainmodel.Course;
import lili.domainmodel.Student;
import lili.model.AttendanceEntryModel;

/**
 * This class is the service module for attendance record related services.
 * 
 * @author Shuangli Wu
 * @NetID sxw133730
 * @date 03/25/2014
 * @purpose CS6359-002 Assignment4
 */
public class AttendanceRecordService extends BaseService {

	/** Data acess objects */
	AttendanceRecordDao attendanceRecordDao = new AttendanceRecordDao();
	CourseDao courseDao = new CourseDao();

	/**
	 * Create a new attendance record from given course and date
	 * 
	 * @param course
	 *            given course
	 * @param dateStr
	 *            given date string
	 * @return an empty attendance record object
	 */
	public AttendanceRecord createNewAttendanceRecord(Course course,
			String dateStr) {
		if (attendanceRecordDao.attendanceRecordFileExists(
				course.getCourseId(), dateStr))
			return null;

		List<Student> students = loadStudentsOfCourse(course);
		List<AttendanceEntryModel> entries = new ArrayList<AttendanceEntryModel>();
		for (Student student : students) {
			AttendanceEntryModel am = new AttendanceEntryModel(student);
			entries.add(am);
		}

		AttendanceRecord result = new AttendanceRecord(course, dateStr, entries);
		return result;
	}

	/**
	 * Save the given attendance record
	 * 
	 * @param ar
	 *            given attendance record
	 * @return whether the attendance record has been sucessfully saved
	 */
	public boolean saveAttendanceRecord(AttendanceRecord ar) {
		return attendanceRecordDao.saveAttendanceRecord(ar);
	}

	public List<String> getAllAttendanceRecordDatesByCourseId(String courseId) {
		List<String> result = new ArrayList<String>();
		File f = new File(Config.RECORD_PREFIX);
		if (!f.exists())
			return result;
		File[] fs = f.listFiles();
		if (fs == null)
			return result;
		for (File r : fs)
			if (r.getName().contains(courseId)) {
				String dateStr = attendanceRecordDao.getDateFromRecFileName(r
						.getName());
				if (dateStr != null)
					result.add(dateStr);
			}
		return result;
	}

	/**
	 * Load the attendance from record files by given courseID and date
	 * 
	 * @param courseId
	 *            given courseId
	 * @param dateStr
	 *            given date
	 * @return
	 */
	public AttendanceRecord loadAttendanceRecord(String courseId, String dateStr) {
		Course course = findCourseById(courseId);

		if (course == null)
			return null;

		List<Student> studentsOfCourse = loadStudentsOfCourse(course);

		return attendanceRecordDao.loadAttendanceRecord(course, dateStr,
				studentsOfCourse);
	}

	/**
	 * Deletes a
	 * 
	 * @param courseId
	 *            given courseId
	 * @param dateStr
	 *            given date
	 * @return whether the record is deleted successfully
	 */
	public boolean deleteAttendanceRecord(String courseId, String dateStr) {
		return attendanceRecordDao.deleteAttendanceRecord(courseId, dateStr);
	}

	/**
	 * This finds a student's attendance records for a specific course
	 * 
	 * @param student
	 *            given student
	 * @param course
	 *            given course
	 * @return a map indicating the student's daily attendance status
	 */
	public HashMap<String, Integer> getStudentAttendanceMap(Student student,
			Course course) {
		LinkedHashMap<String, Integer> result = new LinkedHashMap<String, Integer>();
		File f = new File(Config.RECORD_PREFIX);
		if (!f.exists())
			return result;
		File[] fs = f.listFiles();
		if (fs == null)
			return result;
		for (File r : fs) {
			if (course == null)
				return result;
			if (r.getName().contains(course.getCourseId())) {
				String dateStr = attendanceRecordDao.getDateFromRecFileName(r
						.getName());
				if (dateStr != null) {
					AttendanceRecord ar = loadAttendanceRecord(
							course.getCourseId(), dateStr);
					for (AttendanceEntryModel entry : ar.getEntries()) {
						if (entry.getStudent().getStudentId()
								.equals(student.getStudentId())) {
							String dateStr2 = ar.getDateStr();
							result.put(dateStr2, Integer.valueOf(entry
									.getAttendanceStatus()));
						}
					}
				}
			}
		}
		return result;
	}

	/**
	 * Calculates how many records are present for a specific course
	 * @param course given course
	 * @return the number of records
	 */
	public int getRecordNumOfCourse(Course course) {
		File f = new File(Config.RECORD_PREFIX);
		if (!f.exists())
			return 0;
		File[] fs = f.listFiles();
		return fs == null ? 0 : fs.length;
	}
}
