package lili.service;

import java.util.ArrayList;
import java.util.List;

import lili.dao.CourseDao;
import lili.dao.EnrollmentDao;
import lili.dao.StudentDao;
import lili.domainmodel.Course;
import lili.domainmodel.Enrollment;
import lili.domainmodel.Student;

/**
 * This class is the base service module. It consists of functions to
 * add/delete/update/find entities including students, courses and enrollments.
 * 
 * @author Shuangli Wu
 * @NetID sxw133730
 * @date 03/25/2014
 * @purpose CS6359-002 Assignment4
 */
public class BaseService {

	/** Lower layer DAOs provides primitive data access */
	CourseDao courseDao;
	StudentDao studentDao;
	EnrollmentDao enrollmentDao;

	public BaseService() {
		courseDao = new CourseDao();
		studentDao = new StudentDao();
		enrollmentDao = new EnrollmentDao();

		/** enrollmentDao observes courseDao and studentDao */
		courseDao.addObserver(enrollmentDao);
		studentDao.addObserver(enrollmentDao);
	}

	/**
	 * Find a course by its ID
	 * 
	 * @param courseId
	 *            given courseID
	 * @return the course to be found, or null if the course does not exist
	 */
	public Course findCourseById(String courseId) {
		List<Course> courses = courseDao.loadCourses();
		if (courseId == null)
			return null;

		for (Course course : courses) {
			if (course.getCourseId().equals(courseId))
				return course;
		}

		return null;
	}

	/**
	 * The student to be found
	 * 
	 * @param studentId
	 *            given studentID
	 * @return the student to be found, or null if the student does not exist
	 */
	private Student findStudentById(String studentId) {
		List<Student> students = studentDao.loadStudents();
		if (studentId == null)
			return null;

		for (Student student : students) {
			if (student.getStudentId().equals(studentId))
				return student;
		}

		return null;
	}

	/**
	 * Load all students of a given course
	 * 
	 * @param course
	 *            given course
	 * @return a list of all students enrolled in the course
	 */
	public List<Student> loadStudentsOfCourse(Course course) {
		List<Enrollment> enrollments = enrollmentDao.loadEnrollments();
		List<Student> result = new ArrayList<Student>();
		for (Enrollment e : enrollments) {
			if (e.getCourseId().equals(course.getCourseId())) {
				Student s = findStudentById(e.getStudentId());
				if (s != null)
					result.add(s);
			}
		}
		return result;
	}

	/**
	 * Load all courses on file
	 * 
	 * @return a list of all courses
	 */
	public List<Course> loadCourses() {
		return courseDao.loadCourses();
	}

	/**
	 * Load all students on file
	 * 
	 * @return a list of all students
	 */
	public List<Student> loadStudents() {
		return studentDao.loadStudents();
	}

	/**
	 * Add a course by its ID and name
	 * 
	 * @param courseId
	 *            ID of the course to add
	 * @param courseName
	 *            name of the course to add
	 * @return true if the course is successfully added or false if the course
	 *         already exists
	 */
	public boolean addCourse(String courseId, String courseName) {
		Course course = findCourseById(courseId);
		if (course == null)
			return courseDao.addCourse(courseId, courseName);
		return false;
	}

	/**
	 * Add a student by ID, name and image
	 * 
	 * @param studentId
	 *            ID of the student to add
	 * @param firstName
	 *            first name of the student to add
	 * @param lastName
	 *            last name of the student to add
	 * @param imagePath
	 *            to path to te student's image
	 * @return true if the student is successfully added or false if the student
	 *         already exists
	 */
	public boolean addStudent(String studentId, String firstName,
			String lastName, String imagePath) {
		Student student = findStudentById(studentId);
		if (student == null)
			return studentDao.addStudent(studentId, firstName, lastName,
					imagePath);
		return false;
	}

	/**
	 * Add an enrollment by student ID and course ID
	 * 
	 * @param studentId
	 *            enrollment's studentID
	 * @param courseId
	 *            enrollment's courseID
	 * @return true if the enrollment is successfully added or false if the
	 *         enrollment already exists
	 */
	public boolean addEnrollment(String studentId, String courseId) {
		Enrollment existingEnrollment = enrollmentDao.getEnrollment(studentId,
				courseId);
		if (existingEnrollment != null)
			return false;
		Student student = findStudentById(studentId);
		Course course = findCourseById(courseId);
		if (student != null && course != null) {
			return enrollmentDao.addEnrollment(studentId, courseId);
		}
		return false;
	}

	/**
	 * Delete a course by its "row" index in the course data file.
	 * 
	 * @param row
	 *            the row index of the course information
	 * @return true if the course is successfully deleted or false if the
	 *         deletion fails
	 */
	public boolean deleteCourseByRow(int row) {

		List<Course> courses = courseDao.loadCourses();
		if (courses.size() > row) {
			String courseId = courses.get(row).getCourseId();
			// Delete related enrollments, they will be invalid once the course
			// is removed
			List<Enrollment> enrollments = enrollmentDao.loadEnrollments();
			for (Enrollment e : enrollments) {
				if (e.getCourseId().equals(courseId))
					enrollmentDao.deleteEnrollment(e);
			}
			// Delete course
			return courseDao.deleteCourseById(courseId);
		}
		return false;
	}

	/**
	 * Delete a student by its row index in the student data file.
	 * 
	 * @param row
	 *            the row index of the student information
	 * @return true if the student is successfully deleted or false if the
	 *         deletion fails
	 */
	public boolean deleteStudentByRow(int row) {

		List<Student> students = studentDao.loadStudents();
		if (students.size() > row) {
			String studentId = students.get(row).getStudentId();
			return studentDao.deleteStudentById(studentId);
		}
		return false;
	}

	/**
	 * Delete an enrollment entry by studentID and courseID
	 * 
	 * @param studentId
	 *            the studentID of the enrollment
	 * @param courseId
	 *            the courseID of the enrollment
	 * @return true if the enrollment is successfully deleted or false if the
	 *         deletion fails
	 */
	public boolean deleteEnrollment(String studentId, String courseId) {
		Enrollment enrollment = enrollmentDao
				.getEnrollment(studentId, courseId);
		if (enrollment == null)
			return false;
		return enrollmentDao.deleteEnrollment(enrollment);
	}

	/**
	 * Update courses data using provided data (all courses)
	 * 
	 * @param newCoursesData
	 *            given courses data
	 * @return true if the courses data is successfully updated or false if the
	 *         upate fails
	 */
	public boolean updateCourses(List<String[]> newCoursesData) {
		return courseDao.saveCoursesData(newCoursesData);
	}

	/**
	 * Update students data using provided data (all students)
	 * 
	 * @param newStudentsData
	 *            given students data
	 * @return true if the students data is successfully updated or false if the
	 *         upate fails
	 */
	public boolean updateStudents(List<String[]> newStudentsData) {
		return studentDao.saveStudentsData(newStudentsData);
	}

	public boolean coursesDataChanged(List<String[]> newCoursesData) {
		List<String[]> coursesData = courseDao.loadCoursesDataList();
		for (int index = 0; index < coursesData.size(); index++) {
			for (int col = 0; col < coursesData.get(index).length; col++) {
				if (!coursesData.get(index)[col].equals(newCoursesData
						.get(index)[col])) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Determine whether the given student data is different from the data on
	 * file
	 * 
	 * @param newStudentsData
	 *            given students data
	 * @return true if the data has been changed
	 */
	public boolean studentsDataChanged(List<String[]> newStudentsData) {
		List<String[]> studentsData = studentDao.loadStudentsDataList();
		for (int index = 0; index < studentsData.size(); index++) {
			for (int col = 0; col < studentsData.get(index).length; col++) {
				if (!studentsData.get(index)[col].equals(newStudentsData
						.get(index)[col])) {
					return true;
				}
			}
		}
		return false;
	}
}
