package lili.service;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import lili.Config;
import lili.dao.AttendanceRecordDao;
import lili.dao.CourseDao;
import lili.dao.EnrollmentDao;
import lili.dao.StudentDao;
import lili.domainmodel.AttendanceRecord;
import lili.domainmodel.Course;
import lili.domainmodel.Student;
import lili.model.AttendanceEntryModel;

/**
 * This class contains services for report view. It has 2 functions, detailed
 * report for each student and summary report for each class
 * 
 * @author Shuangli Wu
 * @NetID sxw133730
 * @date 03/25/2014
 * @purpose CS6359-002 Assignment4
 */
public class ReportService extends BaseService {

	/** Needed data access objects */
	AttendanceRecordDao attendanceRecordDao = new AttendanceRecordDao();
	CourseDao courseDao = new CourseDao();
	StudentDao studentDao = new StudentDao();
	EnrollmentDao enrollmentDao = new EnrollmentDao();

	/** Also need the help from attendance service */
	AttendanceRecordService attendanceRecordService = new AttendanceRecordService();

	/**
	 * Generates a map containing the daily student attendance data
	 * 
	 * @param course
	 *            given course
	 * @return a map where entry has <date, 4 different kinds of status counts
	 *         and each entry is for one day
	 */
	public HashMap<String, Integer[]> getStudentAttendanceDailySummary(
			Course course) {
		LinkedHashMap<String, Integer[]> result = new LinkedHashMap<String, Integer[]>();

		File f = new File(Config.RECORD_PREFIX);
		if (!f.exists())
			return result;
		File[] fs = f.listFiles();
		if (fs == null)
			return result;
		for (File recFile : fs) {
			Integer[] dayAttendance = new Integer[4];
			for (int i = 0; i < dayAttendance.length; i++)
				dayAttendance[i] = 0;
			String date = attendanceRecordDao.getDateFromRecFileName(recFile
					.getName());
			if (date != null)
				result.put(date, dayAttendance);
		}

		List<Student> students = loadStudentsOfCourse(course);
		for (Student student : students) {
			HashMap<String, Integer> detailAttendance = attendanceRecordService
					.getStudentAttendanceMap(student, course);
			if (detailAttendance != null) {
				for (String date : detailAttendance.keySet()) {
					int status = detailAttendance.get(date);
					result.get(date)[status]++;
				}
			}
		}

		return result;
	}

	/**
	 * This finds a student's attendance records for a specific course
	 * 
	 * @param student
	 *            given student
	 * @param course
	 *            given course
	 * @return a list indicating the student's daily attendance status
	 */
	public List<Integer> getStudentAttendance(Student student, Course course) {
		List<Integer> result = new ArrayList<Integer>();
		File f = new File(Config.RECORD_PREFIX);
		if (!f.exists())
			return result;
		File[] fs = f.listFiles();
		if (fs == null)
			return result;
		for (File r : fs) {
			if (r.getName().contains(course.getCourseId())) {
				String dateStr = attendanceRecordDao.getDateFromRecFileName(r
						.getName());
				if (dateStr != null) {
					AttendanceRecord ar = attendanceRecordService
							.loadAttendanceRecord(course.getCourseId(), dateStr);
					for (AttendanceEntryModel entry : ar.getEntries()) {
						if (entry.getStudent().getStudentId()
								.equals(student.getStudentId())) {
							result.add(Integer.valueOf(entry
									.getAttendanceStatus()));
						}
					}
				}
			}
		}
		return result;
	}

}
