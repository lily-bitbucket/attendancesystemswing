package lili.model;

import java.util.List;

import javax.swing.table.DefaultTableModel;

/**
 * This class is the model for the whole attendance table, it extends
 * DefaultTableModel
 * 
 * @author Shuangli Wu
 * @NetID sxw133730
 * @date 03/25/2014
 * @purpose CS6359-002 Assignment4
 */
public class AttendanceTableModel extends DefaultTableModel {
	private static final long serialVersionUID = 1L;

	/** How many columns in the attendance table */
	public static int ATTENDANCETABLE_COL_NUM = 7;

	/**
	 * This is important since it will be set outside of this table model, this
	 * is necessary since we are dynamically setting the size of data from this
	 * table
	 */
	public static int dataLen = 0;

	/** A list of attendance entry objects */
	List<AttendanceEntryModel> data;

	/** Constructor */
	public AttendanceTableModel(List<AttendanceEntryModel> data) {
		this.data = data;
	}

	/** Get the object type of a column */
	public Class<?> getColumnClass(int columnIndex) {
		return AttendanceEntryModel.class;
	}

	/** Count the number of columns */
	public int getColumnCount() {
		return ATTENDANCETABLE_COL_NUM;
	}

	/**
	 * Get the column name of a column, we return null because we don't need
	 * column name
	 */
	public String getColumnName(int columnIndex) {
		return null;
	}

	/**
	 * Count the number of rows in the table
	 */
	public int getRowCount() {
		return (int) Math.ceil(1.0 * dataLen / ATTENDANCETABLE_COL_NUM);
	}

	/**
	 * Get the cell object for each cell in the table model
	 */
	public Object getValueAt(int rowIndex, int columnIndex) {
		int offset = rowIndex * ATTENDANCETABLE_COL_NUM + columnIndex;
		if (offset < data.size())
			return data.get(offset);
		return null;
	}

	/**
	 * Return true since all cells are editable
	 */
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return true;
	}
}
