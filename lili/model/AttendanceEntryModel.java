package lili.model;

import lili.domainmodel.AttendanceRecord;
import lili.domainmodel.Student;

/**
 * This class is the model for each cell in attendance table. It has student
 * information and attendance info
 * 
 * @author Shuangli Wu
 * @NetID sxw133730
 * @date 03/25/2014
 * @purpose CS6359-002 Assignment4
 */
public class AttendanceEntryModel {

	/** Attendance status */
	private int attendanceStatus;
	/** Student object */
	private Student student;

	/** Constructor to construct model with student data and unrecorded status */
	public AttendanceEntryModel(Student student) {
		this.student = student;
		this.attendanceStatus = AttendanceRecord.UNRECORDED;
	}

	/** Constructor to construct model with student data and a specified status */
	public AttendanceEntryModel(Student student, int attendanceStatus) {
		this.student = student;
		this.attendanceStatus = attendanceStatus;
	}

	/**
	 * @return the attendanceStatus
	 */
	public int getAttendanceStatus() {
		return attendanceStatus;
	}

	/**
	 * @param attendanceStatus
	 *            the attendanceStatus to set
	 */
	public void setAttendanceStatus(int attendanceStatus) {
		this.attendanceStatus = attendanceStatus;
	}

	/**
	 * @return the student
	 */
	public Student getStudent() {
		return student;
	}

	/**
	 * @param student
	 *            the student to set
	 */
	public void setStudent(Student student) {
		this.student = student;
	}

}
