package lili;

import java.awt.EventQueue;

import javax.swing.JDialog;
import javax.swing.UIManager;

import lili.view.DashboardView;

/**
 * This class defines application wide constants
 * 
 * @author Shuangli Wu
 * @NetID sxw133730
 * @date 03/25/2014
 * @purpose CS6359-002 Assignment4
 */
public class AttendanceApp implements Runnable {

	/**
	 * Start the application
	 * @param args
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new AttendanceApp());
	}

	/**
	 * Initialize and display the dashboard.
	 */
	public void run() {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}

		DashboardView dashboardView = new DashboardView();
		
		dashboardView.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		dashboardView.setResizable(false);
		dashboardView.pack();
		dashboardView.setLocationRelativeTo(null);
		dashboardView.setVisible(true);
	}

}
