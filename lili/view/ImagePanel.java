package lili.view;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

/**
 * This class is a helper panel class for holding/drawing a given student image.
 * 
 * @author Shuangli Wu
 * @NetID sxw133730
 * @date 03/25/2014
 * @purpose CS6359-002 Assignment4
 */
public class ImagePanel extends JPanel {
	
	private static final long serialVersionUID = 1L;

	/** Define the dimension of student's image */
	public static int STUDENT_IMAGE_WIDTH = 80;
	public static int STUDENT_IMAGE_HEIGHT = 80;

	/** The image object to draw */
	private Image image;

	/**
	 * Constructor, set the size of the image and reads the image from the given
	 * path to the image object
	 */
	public ImagePanel(String imagePath, int width, int height) {
		this.setPreferredSize(new Dimension(width, height));
		try {
			BufferedImage bi = ImageIO
					.read(new File("data/avatar/" + imagePath));
			image = bi.getScaledInstance(STUDENT_IMAGE_WIDTH,
					STUDENT_IMAGE_WIDTH, Image.SCALE_SMOOTH);
		} catch (IOException ex) {
			System.out.println("Error in accessing image file.");
			// ex.printStackTrace();
		}
	}

	/**
	 * Override the super class paint method to draw the image
	 */
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		if (image != null)
			g.drawImage(image, 0, 0, null);
	}

}
