package lili.view;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTable;

import lili.domainmodel.AttendanceRecord;
import lili.model.AttendanceEntryModel;

/**
 * This class defines the cell level UI and event handlers for each cell in the
 * attendance recording view. Each cell has 4 radio buttons for student status,
 * it also displays student name and image
 * 
 * @author Shuangli Wu
 * @NetID sxw133730
 * @date 03/25/2014
 * @purpose CS6359-002 Assignment4
 */
public class AttendanceCellComponent extends JPanel {

	private static final long serialVersionUID = 1L;

	/** Dimension of the cell of each student's attendance */
	public static int MAINTABLE_CELL_WIDTH = 80;
	public static int MAINTABLE_CELL_HEIGHT = 220;

	/** UI elements */
	ImagePanel imagePanel;
	JRadioButton btnSetAttended;
	JRadioButton btnSetAbsent;
	JRadioButton btnSetExcused;
	JRadioButton btnSetUnrecorded;
	ButtonGroup buttonGroup;
	JPanel panelButtons;
	JPanel studentInfoPanel;
	JLabel lblStudentName;

	/** Model of each cell, needed to redraw/update the cell content */
	AttendanceEntryModel aem;

	/** Constructor */
	public AttendanceCellComponent() {
		initLayout();
		initHandlers();
		udpateLayout();
	}

	/** Initialize how each cell looks like */
	private void initLayout() {
		this.setLayout(new BorderLayout());
		this.setPreferredSize(new Dimension(MAINTABLE_CELL_WIDTH,
				MAINTABLE_CELL_HEIGHT));

		btnSetAttended = new JRadioButton("Attended");
		btnSetAbsent = new JRadioButton("Absent");
		btnSetExcused = new JRadioButton("Excused");
		btnSetUnrecorded = new JRadioButton("Unrecorded");
		buttonGroup = new ButtonGroup();
		panelButtons = new JPanel();
		studentInfoPanel = new JPanel();
		lblStudentName = new JLabel();

		btnSetAttended.setPreferredSize(new Dimension(100, 25));
		btnSetAbsent.setPreferredSize(new Dimension(100, 25));
		btnSetExcused.setPreferredSize(new Dimension(100, 25));
		btnSetUnrecorded.setPreferredSize(new Dimension(100, 25));

		buttonGroup.add(btnSetAttended);
		buttonGroup.add(btnSetAbsent);
		buttonGroup.add(btnSetExcused);
		buttonGroup.add(btnSetUnrecorded);

		panelButtons.add(btnSetAttended);
		panelButtons.add(btnSetAbsent);
		panelButtons.add(btnSetExcused);
		panelButtons.add(btnSetUnrecorded);
		panelButtons.setPreferredSize(new Dimension(
				ImagePanel.STUDENT_IMAGE_WIDTH, 120));

		studentInfoPanel.add(lblStudentName);
		studentInfoPanel.setPreferredSize(new Dimension(
				ImagePanel.STUDENT_IMAGE_WIDTH, 80));

		this.add(studentInfoPanel, BorderLayout.CENTER);
	}

	/**
	 * Initialize event handlers for radio buttons, each button will set the
	 * cell model's attendance status
	 */
	private void initHandlers() {
		btnSetAttended.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (aem != null)
					aem.setAttendanceStatus(AttendanceRecord.ATTENDED);
			}
		});
		btnSetAbsent.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (aem != null)
					aem.setAttendanceStatus(AttendanceRecord.ABSENT);
			}
		});
		btnSetExcused.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (aem != null)
					aem.setAttendanceStatus(AttendanceRecord.EXCUSED);
			}
		});
		btnSetUnrecorded.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (aem != null)
					aem.setAttendanceStatus(AttendanceRecord.UNRECORDED);
			}
		});
	}

	/**
	 * This function updates the components in the current cell, based on the
	 * cell model updated by function updateData()
	 */
	private void udpateLayout() {
		if (aem != null) {
			// update imagePanel
			if (hasComponent(studentInfoPanel, imagePanel))
				studentInfoPanel.remove(imagePanel);
			imagePanel = new ImagePanel(aem.getStudent().getImagePath(),
					ImagePanel.STUDENT_IMAGE_WIDTH,
					ImagePanel.STUDENT_IMAGE_HEIGHT);
			studentInfoPanel.add(imagePanel);
			// update name label
			lblStudentName.setText(aem.getStudent().getFirstName() + " "
					+ aem.getStudent().getLastName());

			// add panelButtons if necessary
			if (!hasComponent(this, panelButtons))
				this.add(panelButtons, BorderLayout.SOUTH);
		} else {
			if (imagePanel != null) {
				studentInfoPanel.remove(imagePanel);
				lblStudentName.setText("");
			}
			// remove button panel, if I have them
			if (hasComponent(this, panelButtons))
				this.remove(panelButtons);
		}

		if (aem != null)
			setAttendanceChoice(aem.getAttendanceStatus());
	}

	/**
	 * Helper function to determine which radio button should be selected based
	 * on attendance status
	 * 
	 * @param status
	 *            given status
	 */
	public void setAttendanceChoice(int status) {
		if (status == AttendanceRecord.ATTENDED)
			btnSetAttended.setSelected(true);
		if (status == AttendanceRecord.ABSENT)
			btnSetAbsent.setSelected(true);
		if (status == AttendanceRecord.EXCUSED)
			btnSetExcused.setSelected(true);
		if (status == AttendanceRecord.UNRECORDED)
			btnSetUnrecorded.setSelected(true);
	}

	/**
	 * Helper function to determine whether a panel contains a target swing
	 * component
	 * 
	 * @param panel
	 *            given panel
	 * @param target
	 *            given component
	 * @return whether the given panel contains the given component
	 */
	public boolean hasComponent(JPanel panel, Component target) {
		for (Component c : panel.getComponents()) {
			if (c.equals(target))
				return true;
		}
		return false;
	}

	/**
	 * This function can be called by the cell editor and cell renderer, so that
	 * cell data can be updated. It calls update layout, so that the cell can be
	 * updated based on its content (if it is changed)
	 * 
	 * @param aem
	 *            cell model
	 * @param isSelected
	 *            is current cell selected
	 * @param table
	 *            the parenet table
	 */
	public void updateData(AttendanceEntryModel aem, boolean isSelected,
			JTable table) {
		this.aem = aem;
		udpateLayout();
	}

}
