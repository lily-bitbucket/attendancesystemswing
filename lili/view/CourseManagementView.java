package lili.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import lili.service.BaseService;
import lili.service.TableModelService;
import lili.util.SwingHelper;

/**
 * This class defines the view and handlers for the course management GUI.
 * 
 * @author Shuangli Wu
 * @NetID sxw133730
 * @date 03/25/2014
 * @purpose CS6359-002 Assignment4
 */
public class CourseManagementView extends JDialog {

	private static final long serialVersionUID = 1L;

	/** Necessary service objects */
	BaseService baseService = new BaseService();
	TableModelService tableModelService = new TableModelService();

	/** UI elements */
	JButton btnAdd;
	JButton btnDelete;
	JButton btnModify;
	JButton btnCancel;
	JLabel labelCourseID;
	JLabel labelCourseName;
	JTextField ci;
	JTextField cn;
	JTable table;
	JPanel result;

	/** Constructor */
	public CourseManagementView(Window parent) {
		super(parent);
		this.setTitle("Course Management");
		this.setModal(true);
		initLayout();
		initHandlers();
	}

	/** Initialize how each UI element looks like */
	public void initLayout() {
		this.setPreferredSize(new Dimension(450, 300));
		getRootPane().setBorder(new EmptyBorder(10, 10, 10, 10));
		JPanel input = new JPanel(new FlowLayout());
		btnAdd = new JButton("Add");
		btnDelete = new JButton("Delete");
		btnModify = new JButton("Modify");
		btnCancel = new JButton("Close");
		labelCourseName = new JLabel("CourseName:");
		cn = new JTextField();
		cn.setPreferredSize(new Dimension(100, 20));
		labelCourseID = new JLabel("CourseID:");
		ci = new JTextField();
		ci.setPreferredSize(new Dimension(100, 20));
		JPanel inputCharacter = new JPanel();
		inputCharacter.add(labelCourseID);
		inputCharacter.add(ci);
		inputCharacter.add(labelCourseName);
		inputCharacter.add(cn);
		inputCharacter.add(btnAdd);

		table = new JTable();
		result = new JPanel();
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setPreferredSize(new Dimension(400, 160));
		result.add(scrollPane, BorderLayout.CENTER);

		JPanel t = new JPanel();
		t.setLayout(new BorderLayout());
		t.add(input, BorderLayout.NORTH);
		t.add(inputCharacter, BorderLayout.SOUTH);
		this.add(t, BorderLayout.NORTH);
		this.add(result, BorderLayout.CENTER);

		t = new JPanel();
		t.add(btnModify);
		t.add(btnDelete);
		t.add(btnCancel);
		this.add(t, BorderLayout.SOUTH);

		// Initialize the course table with all course data */
		DefaultTableModel tm = tableModelService.getCourseTableModel();
		if (tm != null) {
			table.setModel(tm);
		}
	}

	/**
	 * Initialize handlers for add/delete/modify/cancel buttons
	 */
	public void initHandlers() {

		// Add a course with course information, verify all textfields first
		btnAdd.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				if (SwingHelper.validateNotEmpty(ci.getText())
						&& SwingHelper.validateNotEmpty(cn.getText())) {
					if (!baseService.addCourse(ci.getText(), cn.getText())) {
						SwingHelper.badRequest();
						return;
					}
					DefaultTableModel tm = tableModelService
							.getCourseTableModel();
					if (tm != null) {
						table.setModel(tm);
					}
					SwingHelper.addSuccess();
					cn.setText(null);
					ci.setText(null);
				} else {
					SwingHelper.invalidInputFormat();
				}
			}
		});

		// Delete a course by selecting a row and delete that row
		btnDelete.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				int row = table.getSelectedRow();
				if (row != -1) {
					if (!baseService.deleteCourseByRow(row)) {
						SwingHelper.badRequest();
						return;
					}
					DefaultTableModel tm = tableModelService
							.getCourseTableModel();
					if (tm != null) {
						table.setModel(tm);
					}
					SwingHelper.deleteSuccess();
				} else {
					SwingHelper.noRowSelected();
				}
			}
		});

		// This modify function supports multiple modifications and checking
		// whether modifications exist
		btnModify.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				List<String[]> coursesData = new ArrayList<String[]>();
				for (int row = 0; row < table.getRowCount(); row++) {
					String courseId = (String) table.getValueAt(row, 0);
					String courseName = (String) table.getValueAt(row, 1);
					if (SwingHelper.validateNotEmpty(courseId)
							&& SwingHelper.validateNotEmpty(courseName)) {
						coursesData.add(new String[] { courseId, courseName });
					} else {
						SwingHelper.invalidInputFormat();
					}
				}

				if (baseService.coursesDataChanged(coursesData)) {
					if (!baseService.updateCourses(coursesData)) {
						SwingHelper.badRequest();
						return;
					}
					DefaultTableModel tm = tableModelService
							.getCourseTableModel();
					if (tm != null) {
						table.setModel(tm);
					}
					SwingHelper.updateSuccess();
				} else {
					SwingHelper.noChanges();
				}
			}
		});

		// Cancel by closing the dialog
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Window w = SwingUtilities.getWindowAncestor(btnCancel);
				w.setVisible(false);
				w.dispose();
			}
		});
	}
}
