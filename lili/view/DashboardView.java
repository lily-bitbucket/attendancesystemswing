package lili.view;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

/**
 * This class defines the main entry of all functions, it has a number of
 * buttons, each launches a module's UI.
 * 
 * @author Shuangli Wu
 * @NetID sxw133730
 * @date 03/25/2014
 * @purpose CS6359-002 Assignment4
 */
public class DashboardView extends JFrame {

	private static final long serialVersionUID = 1L;

	/** UI elements */
	private JButton btnCourseMng;
	private JButton btnStudentMng;
	private JButton btnEnrollmentMng;
	private JButton btnRecordAttendance;
	private JButton btnUpdateAttendance;
	private JButton btnReportDetail;
	private JButton btnReportSummary;
	private JPanel attendancePanel;
	private JPanel managementPanel;
	private JPanel reportPanel;

	/** Constructor */
	public DashboardView() {
		initLayout();
		initHandlers();
	}

	/**
	 * Each button handles one simple event: launch the corresponding module
	 */
	private void initHandlers() {
		btnCourseMng.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				Window parentWindow = SwingUtilities
						.windowForComponent(btnCourseMng);
				CourseManagementView cm = new CourseManagementView(parentWindow);
				cm.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				cm.setResizable(false);
				cm.pack();
				cm.setLocationRelativeTo(btnRecordAttendance.getParent());
				cm.setVisible(true);
			}

		});

		btnStudentMng.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				Window parentWindow = SwingUtilities
						.windowForComponent(btnRecordAttendance);
				StudentManagementView sm = new StudentManagementView(
						parentWindow);
				sm.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				sm.setResizable(false);
				sm.pack();
				sm.setLocationRelativeTo(btnRecordAttendance.getParent());
				sm.setVisible(true);

			}

		});

		btnEnrollmentMng.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				Window parentWindow = SwingUtilities
						.windowForComponent(btnRecordAttendance);
				EnrollmentManagementView sm = new EnrollmentManagementView(
						parentWindow);
				sm.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				sm.setResizable(false);
				sm.pack();
				sm.setLocationRelativeTo(btnRecordAttendance.getParent());
				sm.setVisible(true);

			}

		});

		btnRecordAttendance.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				Window parentWindow = SwingUtilities
						.windowForComponent(btnRecordAttendance);
				RecordAttendanceView aView = new RecordAttendanceView(
						parentWindow);
				aView.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				aView.setResizable(false);
				aView.pack();
				aView.setLocationRelativeTo(btnRecordAttendance.getParent());
				aView.setVisible(true);
			}
		});

		btnUpdateAttendance.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				Window parentWindow = SwingUtilities
						.windowForComponent(btnUpdateAttendance);
				UpdateAttendanceView aView = new UpdateAttendanceView(
						parentWindow);
				aView.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				aView.setResizable(false);
				aView.pack();
				aView.setLocationRelativeTo(btnUpdateAttendance.getParent());
				aView.setVisible(true);
			}
		});

		btnReportDetail.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Window parentWindow = SwingUtilities
						.windowForComponent(btnRecordAttendance);
				StatDetailView rView = new StatDetailView(parentWindow);
				rView.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				rView.setResizable(false);
				rView.pack();
				rView.setLocationRelativeTo(btnRecordAttendance.getParent());
				rView.setVisible(true);
			}

		});

		btnReportSummary.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Window parentWindow = SwingUtilities
						.windowForComponent(btnRecordAttendance);
				StatSummaryView rView = new StatSummaryView(parentWindow);
				rView.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				rView.setResizable(false);
				rView.pack();
				rView.setLocationRelativeTo(btnRecordAttendance.getParent());
				rView.setVisible(true);
			}

		});

	}

	/**
	 * Initialize how each UI element looks like, it is heavily customized and
	 * can be organized better
	 */
	private void initLayout() {
		this.setPreferredSize(new Dimension(300, 400));
		this.setTitle("Attendance Management");
		this.setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
		this.getRootPane().setBorder(new EmptyBorder(20, 20, 20, 20));

		btnRecordAttendance = new JButton("Record attendance");
		btnUpdateAttendance = new JButton("Update attendance");
		btnCourseMng = new JButton("Course management");
		btnStudentMng = new JButton("Student management");
		btnEnrollmentMng = new JButton("Enrollment management");
		btnReportDetail = new JButton("Detailed report per course and student");
		btnReportSummary = new JButton("Summary report per course");

		Dimension buttonSize = new Dimension(250, 40);
		btnRecordAttendance.setMaximumSize(buttonSize);
		btnUpdateAttendance.setMaximumSize(buttonSize);
		btnCourseMng.setMaximumSize(buttonSize);
		btnStudentMng.setMaximumSize(buttonSize);
		btnEnrollmentMng.setMaximumSize(buttonSize);
		btnReportDetail.setMaximumSize(buttonSize);
		btnReportSummary.setMaximumSize(buttonSize);

		attendancePanel = new JPanel();
		managementPanel = new JPanel();
		reportPanel = new JPanel();

		attendancePanel.setLayout(new BoxLayout(attendancePanel,
				BoxLayout.Y_AXIS));
		managementPanel.setLayout(new BoxLayout(managementPanel,
				BoxLayout.Y_AXIS));
		reportPanel.setLayout(new BoxLayout(reportPanel, BoxLayout.Y_AXIS));

		attendancePanel.setBorder(new EmptyBorder(10, 10, 10, 10));
		managementPanel.setBorder(new EmptyBorder(10, 10, 10, 10));
		reportPanel.setBorder(new EmptyBorder(10, 10, 10, 10));

		JLabel attendancePanelLabel = new JLabel("Record/Edit Attendance");
		attendancePanelLabel.setFont(new Font("Arial", Font.BOLD, 14));
		JLabel managementPanelLabel = new JLabel(
				"Manage student and course data");
		managementPanelLabel.setFont(new Font("Arial", Font.BOLD, 14));
		JLabel reportPanelLabel = new JLabel("View reports");
		reportPanelLabel.setFont(new Font("Arial", Font.BOLD, 14));

		attendancePanel.add(attendancePanelLabel);
		attendancePanel.add(btnRecordAttendance);
		attendancePanel.add(btnUpdateAttendance);

		managementPanel.add(managementPanelLabel);
		managementPanel.add(btnCourseMng);
		managementPanel.add(btnStudentMng);
		managementPanel.add(btnEnrollmentMng);
		managementPanel.add(new JLabel(" "));

		reportPanel.add(reportPanelLabel);
		reportPanel.add(btnReportDetail);
		reportPanel.add(btnReportSummary);
		reportPanel.add(new JLabel(" "));

		this.add(attendancePanel);
		this.add(managementPanel);
		this.add(reportPanel);

	}
}
