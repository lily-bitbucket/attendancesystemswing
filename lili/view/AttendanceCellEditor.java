package lili.view;

import java.awt.Component;

import javax.swing.AbstractCellEditor;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;

import lili.model.AttendanceEntryModel;

/**
 * This class is extends AbstractCellEditor, so that the the cell can be
 * redrawn when its content has changed.
 * 
 * @author Shuangli Wu
 * @NetID sxw133730
 * @date 03/25/2014
 * @purpose CS6359-002 Assignment4
 */
public class AttendanceCellEditor extends AbstractCellEditor implements
		TableCellEditor {
	private static final long serialVersionUID = 1L;

	/** The panel of current cell */
	AttendanceCellComponent panel;

	public AttendanceCellEditor() {
		panel = new AttendanceCellComponent();
	}

	/**
	 * Overriding method. It essentially updates the cell model for each cell in
	 * the table. So each cell can be redrawn.
	 */
	public Component getTableCellEditorComponent(JTable table, Object value,
			boolean isSelected, int row, int column) {
		AttendanceEntryModel aem = (AttendanceEntryModel) value;
		panel.updateData(aem, true, table);
		return panel;
	}

	/**
	 * Overriding method. Returning null for simplicity
	 */
	public Object getCellEditorValue() {
		return null;
	}
}
