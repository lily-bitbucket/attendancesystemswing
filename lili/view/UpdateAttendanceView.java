package lili.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

import lili.domainmodel.AttendanceRecord;
import lili.domainmodel.Course;
import lili.model.AttendanceEntryModel;
import lili.model.AttendanceTableModel;
import lili.service.BaseService;
import lili.service.AttendanceRecordService;
import lili.service.ReportService;
import lili.util.SwingHelper;

/**
 * This class displays a table of cells with student information and attendance
 * choosers, in addition, the user can select an existing record and their
 * attendance can be updated.
 * 
 * @author Shuangli Wu
 * @NetID sxw133730
 * @date 03/25/2014
 * @purpose CS6359-002 Assignment4
 */
public class UpdateAttendanceView extends JDialog {

	private static final long serialVersionUID = 1L;

	/** Necessary service objects */
	BaseService baseService = new BaseService();
	ReportService reportService = new ReportService();
	AttendanceRecordService attendanceRecordService = new AttendanceRecordService();

	/** UI elements */
	private JLabel labelDate;
	private JLabel labelCourse;
	private JComboBox<String> cbCourse;
	private JComboBox<String> cbDate;
	private JButton btnLoad;
	private JButton btnSave;
	private JButton btnCancel;
	private JButton btnDelete;

	/** A table component to display student information */
	private JTable mainTable;

	/** The list of courses on file */
	List<Course> courseList = baseService.loadCourses();

	/** The attendance record object that is being updated */
	private AttendanceRecord ar;

	/** The ID of current course */
	private String currentCourseId;

	/** Date of the current selected course */
	private String currentDate;

	public UpdateAttendanceView(Window parent) {
		super(parent);
		this.setTitle("Record Attendance");
		this.setModal(true);

		courseList = baseService.loadCourses();

		initLayout();
		initHandler();
	}

	/** Initialize how each UI element looks like */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void initLayout() {
		setPreferredSize(new Dimension(750, 600));
		getRootPane().setBorder(new EmptyBorder(10, 10, 10, 10));
		labelDate = new JLabel("Date:");
		labelCourse = new JLabel("CourseID:");
		cbDate = new JComboBox();

		cbCourse = new JComboBox();
		for (Course course : courseList)
			cbCourse.addItem(course.getCourseId());

		btnLoad = new JButton("Load");
		btnSave = new JButton("Save");
		btnCancel = new JButton("Cancel");
		btnDelete = new JButton("Delete");

		JPanel input = new JPanel();
		input.add(labelCourse);
		input.add(cbCourse);
		input.add(labelDate);
		input.add(cbDate);
		input.add(btnLoad);

		mainTable = new JTable();
		JScrollPane scrollPane = new JScrollPane(mainTable);
		scrollPane
				.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setPreferredSize(new Dimension(500, 500));

		this.add(input, BorderLayout.NORTH);
		this.add(scrollPane, BorderLayout.CENTER);

		JPanel t = new JPanel();
		t.add(btnSave);
		t.add(btnDelete);
		t.add(btnCancel);
		this.add(t, BorderLayout.SOUTH);

		// Initialize default selection
		if (cbCourse.getItemCount() != 0)
			cbCourse.setSelectedIndex(0);
		currentCourseId = (String) cbCourse.getSelectedItem();
		refillDateComboBox(currentCourseId);
	}

	private void refillDateComboBox(String courseId) {
		cbDate.removeAllItems();
		List<String> dateList = attendanceRecordService
				.getAllAttendanceRecordDatesByCourseId(courseId);
		for (String dateStr : dateList)
			cbDate.addItem(dateStr);
	}

	/**
	 * This is a helper function to reuse the code for reloading the attendance
	 * data to the table
	 */
	private void load() {
		currentCourseId = (String) cbCourse.getSelectedItem();
		currentDate = (String) cbDate.getSelectedItem();

		for (int index = 0; index < courseList.size(); index++) {
			if (courseList.get(index).getCourseId()
					.equals((String) cbCourse.getSelectedItem())) {
			}
		}
		// System.out.printf(selectedCourse.getCourseId());
		if (cbDate.getSelectedIndex() != -1) {

			// Here an empty attendance record
			ar = attendanceRecordService.loadAttendanceRecord(currentCourseId,
					currentDate);
			if (ar != null) {
				// Provide the size of data --- the number of students
				AttendanceTableModel.dataLen = ar.getEntries().size();
				mainTable.setModel(new AttendanceTableModel(ar.getEntries()));
				mainTable.setDefaultRenderer(AttendanceEntryModel.class,
						new AttendanceCellRenderer());
				mainTable.setDefaultEditor(AttendanceEntryModel.class,
						new AttendanceCellEditor());
				mainTable
						.setRowHeight(AttendanceCellComponent.MAINTABLE_CELL_HEIGHT);
			} else {
				SwingHelper.badRequest();
			}
		}
	}

	/**
	 * Initialize handlers for recording each student's attendance and saving
	 * the attendance data to file
	 */
	public void initHandler() {
		cbCourse.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				String courseId = (String) cbCourse.getSelectedItem();
				refillDateComboBox(courseId);
			}

		});

		// Clicking the load button will list all students in the selected
		btnLoad.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				load();
			}
		});

		// Save the student status that user updated
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (currentCourseId != null && currentDate != null
						&& ar != null) {
					if (!attendanceRecordService.saveAttendanceRecord(ar)) {
						SwingHelper.badRequest();
					} else {
						SwingHelper.saveSuccess();
					}
				}
			}

		});

		// Delete the selected attendance record file
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (currentCourseId != null && currentDate != null) {
					if (!attendanceRecordService.deleteAttendanceRecord(
							currentCourseId, currentDate)) {
						SwingHelper.badRequest();
					} else {
						SwingHelper.deleteSuccess();
						refillDateComboBox(currentCourseId);
						load();
					}
				}
			}

		});

		// Close the dialog
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Window w = SwingUtilities.getWindowAncestor(btnCancel);
				w.setVisible(false);
				w.dispose();
			}

		});
	}

}
