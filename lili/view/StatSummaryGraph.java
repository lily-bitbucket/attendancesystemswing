package lili.view;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.Stroke;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import javax.swing.*;

import lili.domainmodel.AttendanceRecord;

public class StatSummaryGraph extends JPanel {

	private static final long serialVersionUID = -4596625804316687783L;

	/** Number of lines/series supported, although I am only using 1 line */
	private static final int LINE_NUM = 4;
	/** Gap between the frame and border */
	private static final int BORDER_GAP = 60;
	/** Color of the graph */
	private static final Color GRAPH_COLOR = Color.DARK_GRAY;
	/** Setting the stroke */
	private static final Stroke GRAPH_STROKE = new BasicStroke(1f);
	/** Width of the points */
	private static final int GRAPH_POINT_WIDTH = 1;

	/** Data points to draw, it could have multiple lines/series */
	private HashMap<String, Integer[]> data;
	private int maxScore;

	/** Constructor to have data points provided */
	public StatSummaryGraph(HashMap<String, Integer[]> data, int maxScore) {
		this.data = data;
		this.maxScore = maxScore;
	}

	/**
	 * Overriding the paint function. It does: 
	 * - Convert logical data points to graph points with absolute coordinates. 
	 * - Draw X and Y axis (hatches) 
	 * - Draw the lines/points
	 */
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D) g;
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);

		double xScale = 0;
		double yScale = 0;

		if (data.size() == 0)
			return;

		if (data.size() == 1)
			xScale = ((double) getWidth() - 2 * BORDER_GAP);
		else if (data.size() > 1)
			xScale = ((double) getWidth() - 2 * BORDER_GAP) / (data.size() - 1);
		yScale = ((double) getHeight() - 2 * BORDER_GAP) / maxScore;

		List<LinkedHashMap<String, Point>> layers = new ArrayList<LinkedHashMap<String, Point>>();

		for (int line = 0; line < LINE_NUM; line++) {
			LinkedHashMap<String, Point> graphPoints = new LinkedHashMap<String, Point>();
			int entry = 0;
			for (String key : data.keySet()) {
				int x1 = (int) (entry * xScale + BORDER_GAP);
				int y1 = (int) ((maxScore - data.get(key)[line]) * yScale + BORDER_GAP);
				graphPoints.put(key, new Point(x1, y1));
				entry++;
			}
			layers.add(graphPoints);
		}

		// Create x and y axes
		g2.drawLine(BORDER_GAP, getHeight() - BORDER_GAP, BORDER_GAP,
				BORDER_GAP);
		g2.drawLine(BORDER_GAP, getHeight() - BORDER_GAP, getWidth()
				- BORDER_GAP, getHeight() - BORDER_GAP);

		// Create hatch marks for y axis. The marks are the number of students
		for (int total = 0; total <= maxScore; total++) {
			int x0 = BORDER_GAP;
			int x1 = GRAPH_POINT_WIDTH + BORDER_GAP;
			int y0 = getHeight()
					- ((total * (getHeight() - BORDER_GAP * 2)) / maxScore + BORDER_GAP);
			int y1 = y0;
			g2.drawLine(x0, y0, x1, y1);

			// Numbered markers
			if (total % 2 == 0 && total != maxScore)
				g2.drawString(String.valueOf(total), x0 - 20, y0 + 5);
			// Need to show the number of total students
			if (total == maxScore)
				g2.drawString(String.valueOf(total) + "(total)", x0 - 40,
						y0 + 5);
		}

		// Create hatch marks for x axis.
		for (int index = 0; index < data.size() - 1; index++) {
			int x0 = (index + 1) * (getWidth() - BORDER_GAP * 2)
					/ (data.size() - 1) + BORDER_GAP;
			int x1 = x0;
			int y0 = getHeight() - BORDER_GAP;
			int y1 = y0 - GRAPH_POINT_WIDTH;
			g2.drawLine(x0, y0, x1, y1);
		}

		// Draw lines with the graph points
		drawGraphPoints(g2, layers.get(AttendanceRecord.ATTENDED), GRAPH_COLOR,
				GRAPH_COLOR);
	}

	/**
	 * Draw a poler-graph, so we need to draw two vertical lines and fill the
	 * gap between them
	 */
	public void drawGraphPoints(Graphics2D g2,
			LinkedHashMap<String, Point> graphPoints, Color curveColor,
			Color curveStokeColor) {
		Stroke oldStroke = g2.getStroke();
		g2.setColor(curveColor);
		g2.setStroke(GRAPH_STROKE);
		for (String date : graphPoints.keySet()) {
			int x1 = graphPoints.get(date).x;
			int y1 = graphPoints.get(date).y;
			int y2 = getHeight() - BORDER_GAP;
			g2.drawLine(x1, y1, x1, y2);
			g2.drawLine(x1 + 10, y1, x1 + 10, y2);
			g2.drawLine(x1, y1, x1 + 10, y1);

			for (int y = y1; y <= y2; y += 1)
				g2.drawLine(x1 + 1, y, x1 + 9, y);

			g2.drawString(date, x1, y2 + 20);
		}

		g2.setStroke(oldStroke);
	}

	/**
	 * Setting the dimension of the graph
	 */
	@Override
	public Dimension getPreferredSize() {
		return new Dimension(600, 300);
	}
}
