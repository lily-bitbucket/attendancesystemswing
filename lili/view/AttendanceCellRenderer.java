package lili.view;

import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

import lili.model.AttendanceEntryModel;

/**
 * This class is implements AttendanceCellRenderer, so that the the cell can be
 * rendered using the AttendanceCellComponent class.
 * 
 * @author Shuangli Wu
 * @NetID sxw133730
 * @date 03/25/2014
 * @purpose CS6359-002 Assignment4
 */
public class AttendanceCellRenderer implements TableCellRenderer {

	/** The cell component to be rendered */
	AttendanceCellComponent panel;

	/** Constructor */
	public AttendanceCellRenderer() {
		panel = new AttendanceCellComponent();
	}

	/**
	 * Obtain the up-to-date cell component with updated attendance entry model,
	 * so that the panel can be rendered dynamically
	 */
	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column) {
		AttendanceEntryModel aem = (AttendanceEntryModel) value;
		panel.updateData(aem, isSelected, table);
		return panel;
	}
}
