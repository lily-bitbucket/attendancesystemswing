package lili.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import lili.domainmodel.Course;
import lili.domainmodel.Student;
import lili.service.BaseService;
import lili.service.ReportService;

public class StatDetailView extends JDialog {

	private static final long serialVersionUID = 1L;

	/** Panel for choosing the radio buttons of students */
	private JPanel plStudentChooser;
	/** Course selector */
	private JComboBox<String> cbCourse;
	/** Button group of radio buttons of students */
	private ButtonGroup bgStudents;
	/** List of radio buttons, each corresponds to a student */
	private List<JRadioButton> rbStudentList = new ArrayList<JRadioButton>();

	/** Detailed statistics graph component */
	private StatDetailGraph graph;

	/** Necessary service objects */
	BaseService baseService = new BaseService();
	ReportService reportService = new ReportService();

	/** Course list */
	List<Course> courseList;

	/**
	 * The constructor initializes the course list and initialize the layout and
	 * handlers
	 */
	public StatDetailView(Window parent) {
		super(parent);
		this.setModal(true);

		courseList = baseService.loadCourses();

		initLayout();
		initHandlers();
	}

	/** Initialize the layout of UI elements */
	public void initLayout() {
		this.setPreferredSize(new Dimension(600, 400));
		this.setTitle("Detail Report");
		this.getRootPane().setBorder(new EmptyBorder(20, 20, 10, 0));
		bgStudents = new ButtonGroup();

		// Add the student chooser
		plStudentChooser = new JPanel();
		plStudentChooser.setLayout(new BoxLayout(plStudentChooser,
				BoxLayout.PAGE_AXIS));
		JScrollPane scrollPane = new JScrollPane(plStudentChooser);
		scrollPane.setPreferredSize(new Dimension(150, 300));
		this.add(scrollPane, BorderLayout.WEST);

		if (!courseList.isEmpty()) {
			refillStudentChooser(courseList.get(0).getCourseId());
		}

		// Add course chooser
		cbCourse = new JComboBox<String>();
		for (Course course : courseList)
			cbCourse.addItem(course.getCourseId());

		JPanel t = new JPanel();
		t.add(new JLabel("Please choose course:"));
		t.add(cbCourse);
		this.add(t, BorderLayout.SOUTH);

		this.add(t, BorderLayout.NORTH);

		// Draw the graph so it will not show nothing
		redrawGraph(new ArrayList<Integer>());
	}

	/**
	 * Initialize the handlers for UI elements
	 */
	public void initHandlers() {
		cbCourse.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				String courseId = (String) cbCourse.getSelectedItem();
				refillStudentChooser(courseId);
				redrawGraph(new ArrayList<Integer>());
			}

		});
	}

	/**
	 * This method removes and put new radio buttons according to the current
	 * courseID. Also it adds a listener for each radio button (of each
	 * student), so that it will redraw the graph for each student.
	 * 
	 * @param courseId
	 */
	private void refillStudentChooser(String courseId) {
		for (JRadioButton rb : rbStudentList) {
			bgStudents.remove(rb);
			plStudentChooser.remove(rb);
		}

		rbStudentList.clear();

		final Course course = baseService.findCourseById(courseId);
		List<Student> studentList = baseService.loadStudentsOfCourse(course);
		if (!studentList.isEmpty()) {
			for (final Student student : studentList) {
				JRadioButton rb = new JRadioButton(student.getFirstName() + " "
						+ student.getLastName());

				rb.addActionListener(new ActionListener() {

					public void actionPerformed(ActionEvent arg0) {
						List<Integer> data = reportService
								.getStudentAttendance(student, course);
						redrawGraph(data);
					}

				});

				rbStudentList.add(rb);
				bgStudents.add(rb);
				plStudentChooser.add(rb);
			}
		}

		// Very very important!!!!!
		plStudentChooser.revalidate();
	}

	/**
	 * Redraw the graph by revalidating the graph component
	 */
	private void redrawGraph(List<Integer> data) {
		if (graph != null)
			this.remove(graph);
		if (data != null) {
			graph = new StatDetailGraph(data);
			this.add(graph);
			this.revalidate();
		}
	}

}
