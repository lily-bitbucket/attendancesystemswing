package lili.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;

import lili.service.BaseService;
import lili.service.TableModelService;
import lili.util.SwingHelper;

/**
 * This class defines the view and handlers for the student management GUI.
 * 
 * @author Shuangli Wu
 * @NetID sxw133730
 * @date 03/25/2014
 * @purpose CS6359-002 Assignment4
 */
public class StudentManagementView extends JDialog {

	private static final long serialVersionUID = 1L;

	/** Necessary service objects */
	BaseService baseService = new BaseService();
	TableModelService tableModelService = new TableModelService();

	/** UI elements */
	JButton btnAdd;
	JButton btnDelete;
	JButton btnModify;
	JButton btnCancel;
	JLabel labelStudentID;
	JLabel labelFname;
	JLabel labelLname;
	JTextField sID;
	JTextField fName;
	JTextField lName;
	JTable table;
	JPanel result;
	JLabel labelName;
	JTextField cn;
	JLabel labelAvatarName;
	JButton chooseAvatarButton;

	/** Constructor */
	public StudentManagementView(Window parent) {
		super(parent);
		this.setTitle("Student Management");
		this.setModal(true);
		initLayout();
		initHandlers();
	}

	/** Initialize how each UI element looks like */
	public void initLayout() {
		setPreferredSize(new Dimension(750, 600));
		getRootPane().setBorder(new EmptyBorder(10, 10, 10, 10));
		JPanel input = new JPanel(new FlowLayout());
		btnAdd = new JButton("Add");
		btnDelete = new JButton("Delete");
		btnModify = new JButton("Modify");
		btnCancel = new JButton("Close");

		labelStudentID = new JLabel("StudentID:");
		labelFname = new JLabel("Firstname:");
		labelLname = new JLabel("Lastname:");
		labelAvatarName = new JLabel("");
		chooseAvatarButton = new JButton("...");
		chooseAvatarButton.setPreferredSize(new Dimension(30, 20));
		sID = new JTextField();
		fName = new JTextField();
		lName = new JTextField();
		sID.setPreferredSize(new Dimension(60, 20));
		fName.setPreferredSize(new Dimension(60, 20));
		lName.setPreferredSize(new Dimension(60, 20));

		JPanel inputCharacter = new JPanel();
		inputCharacter.add(labelStudentID);
		inputCharacter.add(sID);
		inputCharacter.add(labelFname);
		inputCharacter.add(fName);
		inputCharacter.add(labelLname);
		inputCharacter.add(lName);
		inputCharacter.add(new JLabel("Avatar:"));
		inputCharacter.add(labelAvatarName);
		inputCharacter.add(chooseAvatarButton);
		inputCharacter.add(new JLabel("      "));
		inputCharacter.add(btnAdd);
		input.add(btnDelete);
		input.add(btnModify);

		table = new JTable();
		result = new JPanel();
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setPreferredSize(new Dimension(720, 470));
		result.add(scrollPane, BorderLayout.CENTER);

		this.add(inputCharacter, BorderLayout.NORTH);
		this.add(result, BorderLayout.CENTER);

		JPanel t = new JPanel();
		t.add(btnModify);
		t.add(btnDelete);
		t.add(btnCancel);
		this.add(t, BorderLayout.SOUTH);

		// Initialize the student table with all student data */
		DefaultTableModel tm = tableModelService.getStudentTableModel();
		if (tm != null) {
			table.setModel(tm);
		}

	}

	/**
	 * Initialize handlers for add/delete/modify/cancel buttons
	 */
	public void initHandlers() {

		chooseAvatarButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				JFileChooser chooser = new JFileChooser("data/avatar");
				FileNameExtensionFilter filter = new FileNameExtensionFilter(
						"PNG Images", "png");
				chooser.setFileFilter(filter);
				int returnVal = chooser.showOpenDialog(chooseAvatarButton);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					labelAvatarName
							.setText(chooser.getSelectedFile().getName());
				}
			}
		});

		// Add a student with student information, verify all textfields first
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (SwingHelper.validateNotEmptyNumeric(sID.getText())
						&& SwingHelper.validateNotEmpty(fName.getText())
						&& SwingHelper.validateNotEmpty(lName.getText())) {
					if (!baseService.addStudent(sID.getText(), fName.getText(),
							lName.getText(), labelAvatarName.getText())) {
						SwingHelper.badRequest();
						return;
					}
					DefaultTableModel tm = tableModelService
							.getStudentTableModel();
					if (tm != null) {
						table.setModel(tm);
					}
					SwingHelper.addSuccess();
					sID.setText(null);
					fName.setText(null);
					lName.setText(null);
					labelAvatarName.setText(null);
				} else {
					if (!SwingHelper.validateNotEmptyNumeric(sID.getText()))
						SwingHelper
								.invalidInputFormat("StudentID should be numeric");
					else
						SwingHelper.invalidInputFormat();
				}
			}
		});

		// Delete a student by selecting a row and delete that row
		btnDelete.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				int row = table.getSelectedRow();
				if (row != 0) {
					if (!baseService.deleteStudentByRow(row)) {
						SwingHelper.badRequest();
						return;
					}
					DefaultTableModel tm = tableModelService
							.getStudentTableModel();
					if (tm != null) {
						table.setModel(tm);
					}
					SwingHelper.deleteSuccess();
				} else {
					SwingHelper.noRowSelected();
				}
			}

		});

		// This modify function supports multiple modifications and checking
		// whether modifications exist
		btnModify.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				List<String[]> studentsData = new ArrayList<String[]>();
				for (int row = 0; row < table.getRowCount(); row++) {
					String studentId = (String) table.getValueAt(row, 0);
					String firstName = (String) table.getValueAt(row, 1);
					String lastName = (String) table.getValueAt(row, 2);
					String imagePath = (String) table.getValueAt(row, 3);
					if (SwingHelper.validateNotEmpty(studentId)
							&& SwingHelper.validateNotEmpty(firstName)
							&& SwingHelper.validateNotEmpty(lastName)) {
						studentsData.add(new String[] { studentId, firstName,
								lastName, imagePath });
					} else {
						SwingHelper.invalidInputFormat();
					}
				}

				if (baseService.studentsDataChanged(studentsData)) {
					if (!baseService.updateStudents(studentsData)) {
						SwingHelper.badRequest();
						return;
					}
					DefaultTableModel tm = tableModelService
							.getStudentTableModel();
					if (tm != null) {
						table.setModel(tm);
					}
					SwingHelper.updateSuccess();
				} else {
					SwingHelper.noChanges();
				}
			}
		});

		// Cancel by closing the dialog
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Window w = SwingUtilities.getWindowAncestor(btnCancel);
				w.setVisible(false);
				w.dispose();
			}

		});
	}
}
