package lili.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;
import java.util.List;
import javax.swing.*;
import javax.swing.border.EmptyBorder;

import lili.domainmodel.AttendanceRecord;
import lili.domainmodel.Course;
import lili.model.AttendanceEntryModel;
import lili.model.AttendanceTableModel;
import lili.service.BaseService;
import lili.service.AttendanceRecordService;
import lili.service.TableModelService;
import lili.util.SwingHelper;

/**
 * This class displays a table of cells with student information and attendance
 * choosers, so that their attendance can be recorded and saved.
 * 
 * @author Shuangli Wu
 * @NetID sxw133730
 * @date 03/25/2014
 * @purpose CS6359-002 Assignment4
 */
public class RecordAttendanceView extends JDialog {

	private static final long serialVersionUID = 1L;

	/** Necessary service objects */
	BaseService baseService = new BaseService();
	TableModelService tableModelService = new TableModelService();
	AttendanceRecordService attendanceRecordService = new AttendanceRecordService();

	/** UI elements */
	private JLabel labelDate;
	private JLabel labelCourse;
	private JComboBox<String> cbCourse;
	private JComboBox<String> month;
	private JComboBox<Integer> day;
	private JComboBox<Integer> year;
	private JButton btnListStudents;
	private JButton btnSave;
	private JButton btnCancel;

	/** A table component to display student information */
	private JTable mainTable;

	/** The list of courses on file */
	List<Course> courseList;

	/** The attendance record object that is being updated */
	private AttendanceRecord ar;

	/** Constructor */
	public RecordAttendanceView(Window parent) {
		super(parent);
		this.setTitle("Record Attendance");
		this.setModal(true);

		courseList = baseService.loadCourses();

		initLayout();
		initHandler();
	}

	/** Initialize how each UI element looks like */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void initLayout() {
		setPreferredSize(new Dimension(750, 600));
		getRootPane().setBorder(new EmptyBorder(10, 10, 10, 10));
		labelDate = new JLabel("Date:");
		labelCourse = new JLabel("CourseID:");
		month = new JComboBox<String>();
		day = new JComboBox<Integer>();
		year = new JComboBox<Integer>();
		cbCourse = new JComboBox();
		btnListStudents = new JButton("List students");
		btnSave = new JButton("Save");
		btnCancel = new JButton("Cancel");

		JPanel input = new JPanel();
		input.add(labelDate);
		input.add(month);
		input.add(day);
		input.add(year);
		input.add(labelCourse);
		input.add(cbCourse);
		input.add(btnListStudents);

		populateDateFields();

		for (Course course : courseList)
			cbCourse.addItem(course.getCourseId());

		mainTable = new JTable();
		JScrollPane scrollPane = new JScrollPane(mainTable);
		scrollPane
				.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setPreferredSize(new Dimension(500, 500));
		this.add(input, BorderLayout.NORTH);
		this.add(scrollPane, BorderLayout.CENTER);

		JPanel t = new JPanel();
		t.add(btnSave);
		t.add(btnCancel);
		this.add(t, BorderLayout.SOUTH);
	}

	/**
	 * Initialize handlers for recording each student's attendance and saving
	 * the attendance data to file
	 */
	public void initHandler() {

		/**
		 * Clicking the list button will list all students in the selected
		 * course
		 */
		btnListStudents.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Course selectedCourse = null;
				for (int i = 0; i < courseList.size(); i++) {
					if (courseList.get(i).getCourseId()
							.equals((String) cbCourse.getSelectedItem())) {
						selectedCourse = courseList.get(i);
					}
				}

				// Here an empty attendance record
				ar = attendanceRecordService.createNewAttendanceRecord(
						selectedCourse, getDateString());
				if (ar != null) {
					// Provide the size of data --- the number of students
					AttendanceTableModel.dataLen = ar.getEntries().size();
					mainTable.setModel(new AttendanceTableModel(ar.getEntries()));
					mainTable.setDefaultRenderer(AttendanceEntryModel.class,
							new AttendanceCellRenderer());
					mainTable.setDefaultEditor(AttendanceEntryModel.class,
							new AttendanceCellEditor());
					mainTable
							.setRowHeight(AttendanceCellComponent.MAINTABLE_CELL_HEIGHT);
				} else {
					SwingHelper
							.messageBox("Today's attendance already exists, please use update attendance function.");
				}
			}
		});

		// Save the student status that user recorded (all the selected radio
		// buttons
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (ar != null && cbCourse.getSelectedItem() != null) {
					if (!attendanceRecordService.saveAttendanceRecord(ar)) {
						SwingHelper.badRequest();
					} else {
						SwingHelper.saveSuccess();
					}
				}
			}

		});

		// Close the dialog
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Window w = SwingUtilities.getWindowAncestor(btnCancel);
				w.setVisible(false);
				w.dispose();
			}

		});
	}

	// Populate the comboboxes for choosing the date from. It is set to the
	// current date by default
	private void populateDateFields() {
		month.addItem("Jan");
		month.addItem("Feb");
		month.addItem("Mar");
		month.addItem("Apr");
		month.addItem("May");
		month.addItem("Jun");
		month.addItem("Jul");
		month.addItem("Aug");
		month.addItem("Sep");
		month.addItem("Oct");
		month.addItem("Nov");
		month.addItem("Dec");

		for (int aDay = 1; aDay <= 31; aDay++) {
			day.addItem(aDay);
		}

		for (int aYear = 2014; aYear < 2030; aYear++) {
			year.addItem(aYear);
		}

		Calendar now = Calendar.getInstance();
		month.setSelectedIndex(now.get(Calendar.MONTH));
		day.setSelectedItem(now.get(Calendar.DAY_OF_MONTH));
		year.setSelectedItem(now.get(Calendar.YEAR));

	}

	// Helper function to construct a string of the selected data
	private String getDateString() {
		return month.getSelectedItem() + "-" + day.getSelectedItem() + "-"
				+ year.getSelectedItem();
	}
}
