package lili.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import lili.domainmodel.Course;
import lili.domainmodel.Student;
import lili.service.BaseService;
import lili.service.TableModelService;
import lili.util.SwingHelper;

/**
 * This class defines the view and handlers for the enrollment management GUI.
 * 
 * @author Shuangli Wu
 * @NetID sxw133730
 * @date 03/25/2014
 * @purpose CS6359-002 Assignment4
 */
public class EnrollmentManagementView extends JDialog {

	private static final long serialVersionUID = 1L;

	/** Necessary service objects */
	BaseService baseService = new BaseService();
	TableModelService tableModelService = new TableModelService();

	/** UI elements */
	JButton btnSearch;
	JButton btnShowAll;
	JButton btnAdd;
	JButton btnRemove;
	JButton btnCancel;
	JLabel labelCourseID;
	JLabel labelStudentID;
	JComboBox<String> cbCourses;
	JComboBox<Student> cbStudents;
	JTable table;
	List<Course> courseList;
	List<Student> studentList;

	/** Constructor */
	public EnrollmentManagementView(Window parent) {
		super(parent);
		this.setTitle("Erollment Management");
		this.setModal(true);

		courseList = baseService.loadCourses();
		studentList = baseService.loadStudents();

		initLayout();
		initHandlers();
	}

	/** Initialize how each UI element looks like */
	public void initLayout() {
		setPreferredSize(new Dimension(750, 600));
		getRootPane().setBorder(new EmptyBorder(10, 10, 10, 10));
		btnSearch = new JButton("Search by course");
		btnShowAll = new JButton("Show all");
		labelCourseID = new JLabel("CourseID:");
		labelStudentID = new JLabel("Student:");
		btnAdd = new JButton("Add");
		btnRemove = new JButton("Delete");
		btnCancel = new JButton("Close");

		cbCourses = new JComboBox<String>();
		cbCourses.setPreferredSize(new Dimension(100, 20));
		cbStudents = new JComboBox<Student>();
		cbStudents.setPreferredSize(new Dimension(140, 20));

		JPanel input = new JPanel();
		input.add(btnSearch);
		input.add(btnShowAll);
		input.add(labelCourseID);
		input.add(cbCourses);
		input.add(labelStudentID);
		input.add(cbStudents);
		input.add(btnAdd);
		input.add(btnRemove);

		table = new JTable();
		JPanel result = new JPanel();
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setPreferredSize(new Dimension(720, 470));
		result.add(scrollPane, BorderLayout.CENTER);

		this.add(input, BorderLayout.NORTH);
		this.add(result, BorderLayout.CENTER);

		JPanel t = new JPanel();
		t.add(btnRemove);
		t.add(btnCancel);
		this.add(t, BorderLayout.SOUTH);

		for (Course course : courseList) {
			cbCourses.addItem(course.getCourseId());
		}
		if (cbCourses.getItemCount() > 0)
			cbCourses.setSelectedIndex(0);

		for (Student student : studentList)
			cbStudents.addItem(student);
		if (cbStudents.getItemCount() > 0)
			cbStudents.setSelectedIndex(0);

		DefaultTableModel tm = tableModelService.getEnrollmentTableModel();
		if (tm != null) {
			table.setModel(tm);
		}
	}

	/**
	 * Initialize handlers for search/add/delete/cancel buttons
	 */
	public void initHandlers() {

		// Search button can search for the enrollment entries of a specified
		// course
		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String courseId = (String) cbCourses.getSelectedItem();
				DefaultTableModel tm = tableModelService
						.getEnrollmentTableModelByCourseId(courseId);
				if (tm != null) {
					table.setModel(tm);
				}
			}
		});

		// Show all button can show the enrollment entries of all classes.
		btnShowAll.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				DefaultTableModel tm = tableModelService
						.getEnrollmentTableModel();
				if (tm != null) {
					table.setModel(tm);
				}
			}
		});

		// A new enrollment entry can be added by simply select a student and a
		// course
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Student student = (Student) cbStudents.getSelectedItem();
				String studentId = student.getStudentId();
				String courseId = (String) cbCourses.getSelectedItem();
				if (!baseService.addEnrollment(studentId, courseId)) {
					SwingHelper.badRequest();
					return;
				}
				DefaultTableModel tm = tableModelService
						.getEnrollmentTableModel();
				if (tm != null) {
					table.setModel(tm);
				}
				SwingHelper.addSuccess();

			}

		});
		
		// Remove button removes the selected enrollment entry
		btnRemove.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int row = table.getSelectedRow();
				if (row != -1) {
					String studentId = (String) table.getValueAt(row, 0);
					String courseId = (String) table.getValueAt(row, 1);
					if (!baseService.deleteEnrollment(studentId, courseId)) {
						SwingHelper.badRequest();
						return;
					}

					DefaultTableModel tm = tableModelService
							.getEnrollmentTableModel();
					if (tm != null) {
						table.setModel(tm);
					}
					SwingHelper.deleteSuccess();
				} else {
					SwingHelper.noRowSelected();
				}
			}
		});

		// Cancel button closes the dialog
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Window w = SwingUtilities.getWindowAncestor(btnCancel);
				w.setVisible(false);
				w.dispose();
			}

		});
	}
}
