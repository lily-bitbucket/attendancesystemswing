package lili.view;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.Stroke;
import java.util.ArrayList;
import java.util.List;

import javax.swing.*;

import lili.Config;
import lili.domainmodel.AttendanceRecord;

public class StatDetailGraph extends JPanel {

	private static final long serialVersionUID = 1586690159711908429L;

	/** Data points to draw */
	private List<Integer> data;

	/** Gap between the frame and border */
	private static final int BORDER_GAP = 60;
	/** Color of the lines */
	private static final Color GRAPH_COLOR = new Color(0, 0, 255, 50);
	/** Color of the points */
	private static final Color GRAPH_POINT_COLOR = new Color(255, 0, 0, 200);
	/** Setting the stroke */
	private static final Stroke GRAPH_STROKE = new BasicStroke(3f);
	/** Width of the points */
	private static final int GRAPH_POINT_WIDTH = 5;
	/** Number of Y-axis hatches */
	private static final int Y_HATCH_CNT = Config.STATUS_NUM - 1;

	/** Constructor to have data points provided */
	public StatDetailGraph(List<Integer> data) {
		this.data = data;
	}

	/**
	 * Overriding the paint function. It does:
	 * - Convert logical data points to graph points with absolute coordinates. 
	 * - Draw X and Y axis (hatches)
	 * - Draw the lines/points
	 */
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D) g;
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);

		double xScale = 0;

		// Take care of special cases and calculate the x and y scales
		if (data.size() == 0)
			return;

		if (data.size() == 1)
			xScale = ((double) getWidth() - 2 * BORDER_GAP);
		else if (data.size() > 1)
			xScale = ((double) getWidth() - 2 * BORDER_GAP) / (data.size() - 1);
		double yScale = ((double) getHeight() - 2 * BORDER_GAP)
				/ (Config.STATUS_NUM - 1);

		// Calculate graph points with absolute coordinates
		List<Point> graphPoints = new ArrayList<Point>();
		for (int scoreEntry = 0; scoreEntry < data.size(); scoreEntry++) {
			int x1 = (int) (scoreEntry * xScale + BORDER_GAP);
			int y1 = (int) ((Config.STATUS_NUM - data.get(scoreEntry) - 1)
					* yScale + BORDER_GAP);
			graphPoints.add(new Point(x1, y1));
		}

		// Create x and y axes
		g2.drawLine(BORDER_GAP, getHeight() - BORDER_GAP, BORDER_GAP,
				BORDER_GAP);
		g2.drawLine(BORDER_GAP, getHeight() - BORDER_GAP, getWidth()
				- BORDER_GAP, getHeight() - BORDER_GAP);

		// Create hatch marks for y axis. The marks are for different attendance
		// status
		for (int index = 0; index < Y_HATCH_CNT + 1; index++) {
			int x0 = BORDER_GAP;
			int x1 = GRAPH_POINT_WIDTH + BORDER_GAP;
			int y0 = getHeight()
					- ((index * (getHeight() - BORDER_GAP * 2)) / Y_HATCH_CNT + BORDER_GAP);
			int y1 = y0;
			g2.drawLine(x0, y0, x1, y1);
			g2.drawString(getAttendanceStatusString(index), x0 - 50, y0);
		}

		// Create hatch marks for x axis. 
		for (int index = 0; index < data.size() - 1; index++) {
			int x0 = (index + 1) * (getWidth() - BORDER_GAP * 2)
					/ (data.size() - 1) + BORDER_GAP;
			int x1 = x0;
			int y0 = getHeight() - BORDER_GAP;
			int y1 = y0 - GRAPH_POINT_WIDTH;
			g2.drawLine(x0, y0, x1, y1);
		}

		// Draw lines with the graph points
		Stroke oldStroke = g2.getStroke();
		g2.setColor(GRAPH_COLOR);
		g2.setStroke(GRAPH_STROKE);
		for (int index = 0; index < graphPoints.size() - 1; index++) {
			int x1 = graphPoints.get(index).x;
			int y1 = graphPoints.get(index).y;
			int x2 = graphPoints.get(index + 1).x;
			int y2 = graphPoints.get(index + 1).y;
			g2.drawLine(x1, y1, x2, y2);
		}

		// Draw the circle around each point on the line
		g2.setStroke(oldStroke);
		g2.setColor(GRAPH_POINT_COLOR);
		for (int i = 0; i < graphPoints.size(); i++) {
			int x = graphPoints.get(i).x - GRAPH_POINT_WIDTH / 2;
			int y = graphPoints.get(i).y - GRAPH_POINT_WIDTH / 2;
			;
			int ovalW = GRAPH_POINT_WIDTH;
			int ovalH = GRAPH_POINT_WIDTH;
			g2.fillOval(x, y, ovalW, ovalH);
		}
	}

	/**
	 * Construct the attendance status string
	 * 
	 * @param statusCode
	 *            given status code
	 * @return attendance status string
	 */
	public String getAttendanceStatusString(int statusCode) {
		if (statusCode == AttendanceRecord.ABSENT)
			return "absent";
		if (statusCode == AttendanceRecord.ATTENDED)
			return "attended";
		if (statusCode == AttendanceRecord.EXCUSED)
			return "excused";
		if (statusCode == AttendanceRecord.UNRECORDED)
			return "norecord";
		return null;
	}

	/**
	 * Setting the dimension of the graph
	 */
	@Override
	public Dimension getPreferredSize() {
		return new Dimension(600, 300);
	}
}
