package lili.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

import lili.domainmodel.Course;
import lili.service.BaseService;
import lili.service.ReportService;

public class StatSummaryView extends JDialog {

	private static final long serialVersionUID = 1L;

	/** Course selector */
	private JComboBox<String> cbCourse;
	/** Summary graph component */
	private StatSummaryGraph graph;

	/** Necessary service objects */
	BaseService baseService = new BaseService();
	ReportService reportService = new ReportService();

	/** Course list */
	List<Course> courseList;

	/**
	 * The constructor initializes the course list and initialize the layout and
	 * handlers
	 */
	public StatSummaryView(Window parent) {
		super(parent);
		this.setModal(true);

		courseList = baseService.loadCourses();

		initLayout();
		initHandlers();
	}

	/** Initialize the layout of UI elements */
	public void initLayout() {
		this.setPreferredSize(new Dimension(600, 400));
		this.setTitle("Summary Report");
		// Add course chooser
		cbCourse = new JComboBox<String>();
		for (Course course : courseList)
			cbCourse.addItem(course.getCourseId());

		JPanel t = new JPanel();
		t.add(new JLabel("Please choose course:"));
		t.add(cbCourse);
		this.add(t, BorderLayout.NORTH);

		HashMap<String, Integer[]> emptyList = new HashMap<String, Integer[]>();
		redrawGraph(emptyList, 0);

		if (cbCourse.getItemCount() > 0) {
			cbCourse.setSelectedIndex(0);
			String courseId = (String) cbCourse.getSelectedItem();
			final Course course = baseService.findCourseById(courseId);
			HashMap<String, Integer[]> data = reportService
					.getStudentAttendanceDailySummary(course);
			int num = baseService.loadStudentsOfCourse(course).size();
			redrawGraph(data, num);
		}
	}

	/**
	 * Initialize the handlers for UI elements
	 */
	public void initHandlers() {

		// Add the handler for course selector, it chooses the class and redraw
		// the graph
		cbCourse.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				String courseId = (String) cbCourse.getSelectedItem();
				final Course course = baseService.findCourseById(courseId);
				HashMap<String, Integer[]> data = reportService
						.getStudentAttendanceDailySummary(course);
				int num = baseService.loadStudentsOfCourse(course).size();
				redrawGraph(data, num);
			}

		});
	}

	/**
	 * Redraw the graph by revalidating the graph component
	 */
	private void redrawGraph(HashMap<String, Integer[]> data, int maxScore) {
		if (graph != null)
			this.remove(graph);
		if (data != null && maxScore >= 0) {
			graph = new StatSummaryGraph(data, maxScore);
			this.add(graph);
			this.revalidate();
		}
	}

}
