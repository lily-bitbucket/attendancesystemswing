package lili.domainmodel;

import java.util.List;
import lili.model.AttendanceEntryModel;

/**
 * This class is pojo describing an attendance record, which consists of the
 * course, date, and the students information.
 * 
 * @author Shuangli Wu
 * @NetID sxw133730
 * @date 03/25/2014
 * @purpose CS6359-002 Assignment4
 */
public class AttendanceRecord {

	/** Status code meaning a student's attendance is not recorded */
	public static final int UNRECORDED = 0;
	/** Status code meaning a student has attended a course */
	public static final int ATTENDED = 3;
	/** Status code meaning a student is absent */
	public static final int ABSENT = 1;
	/** Status code meaning a student has been excused for his/her absence */
	public static final int EXCUSED = 2;

	/** The course of current attendance recording */
	private Course course;
	/** The date of current attendance recording in String format */
	private String dateStr;
	/** The entries of current recording */
	private List<AttendanceEntryModel> entries;

	/**
	 * Constructor function. A complete attendance record needs a course object,
	 * a date string, and a set of students
	 */
	public AttendanceRecord(Course course, String dateStr,
			List<AttendanceEntryModel> entries) {
		this.course = course;
		this.dateStr = dateStr;
		this.entries = entries;
	}

	/**
	 * @return the course
	 */
	public Course getCourse() {
		return course;
	}

	/**
	 * @param course
	 *            the course to set
	 */
	public void setCourse(Course course) {
		this.course = course;
	}

	/**
	 * @return the dateStr
	 */
	public String getDateStr() {
		return dateStr;
	}

	/**
	 * @param dateStr
	 *            the dateStr to set
	 */
	public void setDateStr(String dateStr) {
		this.dateStr = dateStr;
	}

	/**
	 * @return the entries
	 */
	public List<AttendanceEntryModel> getEntries() {
		return entries;
	}

	/**
	 * @param entries
	 *            the entries to set
	 */
	public void setEntries(List<AttendanceEntryModel> entries) {
		this.entries = entries;
	}
}
