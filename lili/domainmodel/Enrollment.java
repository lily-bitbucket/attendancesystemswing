package lili.domainmodel;

/**
 * This class is pojo for the enrollment relatioship between students and
 * courses.
 * 
 * @author Shuangli Wu
 * @NetID sxw133730
 * @date 03/25/2014
 * @purpose CS6359-002 Assignment4
 */
public class Enrollment {

	/** student ID */
	String studentId;
	/** course ID */
	String courseId;

	/** Constructor */
	public Enrollment(String studentId, String courseId) {
		this.studentId = studentId;
		this.courseId = courseId;
	}

	/**
	 * 
	 * @return the studentId
	 */
	public String getStudentId() {
		return studentId;
	}

	/**
	 * @param studentId
	 *            the studentId to set
	 */
	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}

	/**
	 * @return the courseId
	 */
	public String getCourseId() {
		return courseId;
	}

	/**
	 * @param courseId
	 *            the courseId to set
	 */
	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}

}
