package lili.domainmodel;

/**
 * This class is pojo describing a student, including his/her name, ID and
 * avatar image path.
 * 
 * @author Shuangli Wu
 * @NetID sxw133730
 * @date 03/25/2014
 * @purpose CS6359-002 Assignment4
 */
public class Student {

	/** studentID */
	private String studentId;
	/** First name */
	private String firstName;
	/** Last name */
	private String lastName;
	/** Path to the image of the student */
	private String imagePath;

	/** Constructor */
	public Student(String studentId, String firstName, String lastName,
			String imagePath) {
		this.studentId = studentId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.imagePath = imagePath;
	}

	/**
	 * @return the studentId
	 */
	public String getStudentId() {
		return studentId;
	}

	/**
	 * @param studentId
	 *            the studentId to set
	 */
	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the imagePath
	 */
	public String getImagePath() {
		return imagePath;
	}

	/**
	 * @param imagePath
	 *            the imagePath to set
	 */
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	/**
	 * This overrides the default toString() method. It will be called by the UI
	 * (e.g., combobox), and display both studentID and name.
	 */
	public String toString() {
		return studentId + " - " + firstName + " " + lastName;
	}

}
