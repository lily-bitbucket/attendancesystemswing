package lili.domainmodel;

/**
 * This class is pojo describing a course, including the ID and name of it.
 * 
 * @author Shuangli Wu
 * @NetID sxw133730
 * @date 03/25/2014
 * @purpose CS6359-002 Assignment4
 */
public class Course {

	/** Course ID, e.g., CS101 */
	private String courseId;
	/** Course name, e.g. OOAD */
	private String courseName;

	/** Constructor */
	public Course(String courseId, String courseName) {
		this.courseId = courseId;
		this.courseName = courseName;
	}

	/**
	 * @return the courseId
	 */
	public String getCourseId() {
		return courseId;
	}

	/**
	 * @param courseId
	 *            the courseId to set
	 */
	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}

	/**
	 * @return the courseName
	 */
	public String getCourseName() {
		return courseName;
	}

	/**
	 * @param courseName
	 *            the courseName to set
	 */
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

}
