package lili;

/**
 * This class defines application wide constants
 * 
 * @author Shuangli Wu
 * @NetID sxw133730 
 * @date 03/25/2014
 * @purpose CS6359-002 Assignment4
 */
public class Config {
	/** Delimiter of classId and date in attendance record file names */
	public static final String REC_FILENAME_DELIMITER = "__";
	
	/** Path prefix for attendance records */
	public static final String RECORD_PREFIX = "data/record/";
	
	/** Path to the text file storing student information */
	public static final String STUDENT_CSV = "data/entity/student.csv";
	
	/** Path to the text file storing enrollment information */
	public static final String ENROLLMENT_CSV = "data/entity/enrollment.csv";
	
	/** Path to the text file storing course information */
	public static final String COURSE_CSV = "data/entity/course.csv";
	
	/** Path to the avatar image files */
	public static final String STUDENT_AVATAR_PREFIX = "data/avatar/";
	
	/** Number of different status */
	public static final int STATUS_NUM = 4;

}
