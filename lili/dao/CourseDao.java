package lili.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import lili.Config;
import lili.domainmodel.Course;
import lili.service.io.DataManager;

/**
 * This class is the data access object for courses. It can save, read and
 * delete course data.
 * 
 * @author Shuangli Wu
 * @NetID sxw133730
 * @date 03/25/2014
 * @purpose CS6359-002 Assignment4
 */
public class CourseDao extends Observable {

	/** A DataManager object is used to access persisted data. */
	public DataManager dataManager = DataManager.getUniqueInstance();

	/**
	 * Load all courses from course data file
	 * 
	 * @return a list of all courses
	 */
	public List<Course> loadCourses() {
		List<Course> result = new ArrayList<Course>();

		try {
			List<String[]> rows = dataManager.readCSV(Config.COURSE_CSV);
			for (String[] row : rows) {
				String courseId = row[0];
				String courseName = row[1];
				Course course = new Course(courseId, courseName);
				result.add(course);
			}
			return result;
		} catch (Exception e) {
			return result;
		}
	}

	/**
	 * Add a course to the course data file
	 * 
	 * @param courseId
	 *            given courseID
	 * @param courseName
	 *            given courseName
	 * @return whether the course has been successfully saved
	 */
	public boolean addCourse(String courseId, String courseName) {
		try {
			dataManager.appendToCSV(Config.COURSE_CSV, new String[] { courseId,
					courseName });
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Load courses data to a 2D string array, raw data only
	 * 
	 * @return the array containing course data
	 */
	public String[][] loadCoursesData() {
		try {
			return dataManager.readCSV2DArray(Config.COURSE_CSV);
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * Load courses data to a list of string array, raw data only
	 * 
	 * @return the list containing course data, each course is an array in the
	 *         list
	 */
	public List<String[]> loadCoursesDataList() {
		try {
			return dataManager.readCSV(Config.COURSE_CSV);
		} catch (Exception e) {
			return new ArrayList<String[]>();
		}
	}

	/**
	 * Save given courses data (all data included)
	 * @param data given data
	 * @return whether data has been successfully saved
	 */
	public boolean saveCoursesData(List<String[]> data) {
		try {
			// This must be called before changing data file
			setChanged();
			notifyObservers(data);
			dataManager.writeCSV(Config.COURSE_CSV, data);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Delete a course by its ID
	 * @param courseId given courseID
	 * @return whether the course has been successfully deleted
	 */
	public boolean deleteCourseById(String courseId) {
		try {
			List<String[]> list = loadCoursesDataList();
			for (int item = 0; item < list.size(); item++) {
				String[] rowData = list.get(item);
				if (rowData[0].equals(courseId))
					list.remove(item);
			}
			setChanged();
			notifyObservers(courseId);
			saveCoursesData(list);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
}
