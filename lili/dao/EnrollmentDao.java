package lili.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import lili.Config;
import lili.domainmodel.Enrollment;
import lili.service.io.DataManager;

/**
 * This class is the data access object for enrollment. It can save, read and
 * delete enrollment data.
 * 
 * @author Shuangli Wu
 * @NetID sxw133730
 * @date 03/25/2014
 * @purpose CS6359-002 Assignment4
 */
public class EnrollmentDao implements Observer {

	/** A DataManager object is used to access persisted data. */
	public DataManager dataManager = DataManager.getUniqueInstance();

	/**
	 * Add an enrollment of given studentID and courseID
	 * 
	 * @param studentId
	 *            given studentID
	 * @param courseId
	 *            given courseId
	 * @return whether the enrollment has been added
	 */
	public boolean addEnrollment(String studentId, String courseId) {
		try {
			dataManager.appendToCSV(Config.ENROLLMENT_CSV, new String[] {
					studentId, courseId });
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Load all the enrollments data from the enrollment data file, and convert
	 * the data to Enrollment objects
	 * 
	 * @return a list of Enrollment data
	 */
	public List<Enrollment> loadEnrollments() {
		List<Enrollment> enrollments = new ArrayList<Enrollment>();
		try {
			List<String[]> data = dataManager.readCSV(Config.ENROLLMENT_CSV);
			for (String[] row : data) {
				Enrollment e = new Enrollment(row[0], row[1]);
				enrollments.add(e);
			}
			return enrollments;
		} catch (Exception e) {
			return enrollments;
		}
	}

	/**
	 * Load all the enrollments data from the enrollment data file, raw data
	 * only
	 * 
	 * @return a list of raw enrollment data
	 */
	public String[][] loadEnrollmentsData() {
		try {
			return dataManager.readCSV2DArray(Config.ENROLLMENT_CSV);
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * Load all the enrollments data from the enrollment data file, raw data
	 * only, to a list of array
	 * 
	 * @return a list of array of raw enrollment data
	 */
	public List<String[]> loadEnrollmentsDataList() {
		try {
			return dataManager.readCSV(Config.ENROLLMENT_CSV);
		} catch (Exception e) {
			return new ArrayList<String[]>();
		}
	}

	/**
	 * Save all enrollments data to file
	 * 
	 * @param data
	 *            given enrollment data
	 * @return whether data has been successfully saved
	 */
	public boolean saveEnrollmentsDataList(List<String[]> data) {
		try {
			dataManager.writeCSV(Config.ENROLLMENT_CSV, data);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Search enrollment data file for an enrollment entry
	 * 
	 * @param studentId
	 *            given student ID
	 * @param courseId
	 *            given course ID
	 * @return the enrollment looking for
	 */
	public Enrollment getEnrollment(String studentId, String courseId) {
		try {
			List<String[]> list = loadEnrollmentsDataList();
			for (int i = 0; i < list.size(); i++) {
				String[] rowData = list.get(i);
				if (rowData[0].equals(studentId) && rowData[1].equals(courseId))
					return new Enrollment(rowData[0], rowData[1]);
			}
			return null;
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * Delete the given enrollment entry
	 * 
	 * @param enrollment
	 *            given enrollment to delete
	 * @return whether the enrollment data has been deleted
	 */
	public boolean deleteEnrollment(Enrollment enrollment) {
		try {
			List<String[]> list = loadEnrollmentsDataList();
			for (int i = 0; i < list.size(); i++) {
				String[] rowData = list.get(i);
				if (rowData[0].equals(enrollment.getStudentId())
						&& rowData[1].equals(enrollment.getCourseId())) {
					list.remove(i);
					break;
				}
			}
			saveEnrollmentsDataList(list);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Update the enrollment with given studentID and courseID
	 * 
	 * @param enrollment
	 *            the enrollment to update
	 * @param studentId
	 *            given studentID
	 * @param courseId
	 *            given courseID
	 * @return whether the enrollment has been successfully updated
	 */
	public boolean updateEnrollment(Enrollment enrollment, String studentId,
			String courseId) {
		try {
			List<String[]> list = loadEnrollmentsDataList();
			for (int i = 0; i < list.size(); i++) {
				String[] rowData = list.get(i);
				if (rowData[0].equals(enrollment.getStudentId())
						&& rowData[1].equals(enrollment.getCourseId()))
					list.set(i, new String[] { studentId, courseId });
			}
			saveEnrollmentsDataList(list);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * This method handles updates from the objects it observes, the updates
	 * includes: - studentDao delete by id - studentDao update - courseDao
	 * delete by id - courseDao update
	 */
	@Override
	public void update(Observable obj, Object arg) {
		// Handle all studentDao related updates
		if (obj instanceof StudentDao) {
			StudentDao studentDao = (StudentDao) obj;
			// arg is studentID, so studentDao deleted a student with this ID
			if (arg instanceof String) {
				String studentId = (String) arg;
				List<Enrollment> enrollments = loadEnrollments();
				// Delete related enrollments, because these enrollments will
				// become invalid
				for (Enrollment e : enrollments) {
					if (e.getStudentId().equals(studentId))
						deleteEnrollment(e);
				}
			}

			// arg is String[][], so studentDao is updating student data
			if (arg instanceof List<?>) {
				@SuppressWarnings("unchecked")
				List<String[]> studentsDataToUpdate = (List<String[]>) arg;
				List<String[]> studentsData = studentDao.loadStudentsDataList();

				// Update all the enrollments including both studentId and
				// courseId
				List<String[]> enrollmentsDataList = loadEnrollmentsDataList();
				for (int index = 0; index < studentsData.size(); index++) {
					String studentIdToUpdate = studentsDataToUpdate.get(index)[0];
					String studentId = studentsData.get(index)[0];
					if (!studentIdToUpdate.equals(studentId)) {
						for (String[] entry : enrollmentsDataList) {
							if (entry[0].equals(studentId)) {
								entry[0] = studentIdToUpdate;
							}
						}
					}
				}
				saveEnrollmentsDataList(enrollmentsDataList);
			}
		}

		// Handle all studentDao related updates
		if (obj instanceof CourseDao) {
			CourseDao courseDao = (CourseDao) obj;
			// arg is courseID, so courseDao deleted a course with this ID
			if (arg instanceof String) {
				String courseId = (String) arg;
				List<Enrollment> enrollments = loadEnrollments();
				for (Enrollment e : enrollments) {
					if (e.getCourseId().equals(courseId))
						deleteEnrollment(e);
				}
			}

			// arg is String[][], so courseDao is updating course data
			if (arg instanceof List<?>) {
				@SuppressWarnings("unchecked")
				List<String[]> coursesDataToUpdate = (List<String[]>) arg;
				List<String[]> coursesData = courseDao.loadCoursesDataList();

				// Update all the enrollments including both courseId and
				// courseId
				List<String[]> enrollmentsDataList = loadEnrollmentsDataList();
				for (int index = 0; index < coursesData.size(); index++) {
					String courseIdToUpdate = coursesDataToUpdate.get(index)[0];
					String courseId = coursesData.get(index)[0];
					if (!courseIdToUpdate.equals(courseId)) {
						for (String[] entry : enrollmentsDataList) {
							if (entry[1].equals(courseId)) {
								entry[1] = courseIdToUpdate;
							}
						}
					}
				}
				saveEnrollmentsDataList(enrollmentsDataList);
			}
		}
	}
}
