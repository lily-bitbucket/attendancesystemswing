package lili.dao;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import lili.Config;
import lili.domainmodel.AttendanceRecord;
import lili.domainmodel.Course;
import lili.domainmodel.Student;
import lili.model.AttendanceEntryModel;
import lili.service.io.DataManager;

/**
 * This class is the data access object for attendance records. It can save,
 * read and delete attendance.
 * 
 * @author Shuangli Wu
 * @NetID sxw133730
 * @date 03/25/2014
 * @purpose CS6359-002 Assignment4
 */
public class AttendanceRecordDao {

	/** A DataManager object is used to access persisted data. */
	public DataManager dataManager = DataManager.getUniqueInstance();

	/**
	 * This method generates a standard record file name based on given courseID
	 * and date string.
	 * 
	 * @param courseId
	 *            ID of a course
	 * @param dateStr
	 *            date string
	 * @return record file name
	 */
	private String genAttendanceRecordFileName(String courseId, String dateStr) {
		return Config.RECORD_PREFIX + courseId + Config.REC_FILENAME_DELIMITER
				+ dateStr + ".csv";
	}

	/**
	 * This method extracts the date string from the record file name
	 * 
	 * @param fileName
	 *            name of the record file
	 * @return date of the record file
	 */
	public String getDateFromRecFileName(String fileName) {
		try {
			return fileName.split("\\.")[0]
					.split(Config.REC_FILENAME_DELIMITER)[1];
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * This method detects whether the record file of the given courseId and
	 * data string exists
	 * 
	 * @param courseId
	 *            ID of a course
	 * @param dateStr
	 *            date string
	 * @return whether the record file exist
	 */
	public boolean attendanceRecordFileExists(String courseId, String dateStr) {
		try {
			String filePath = genAttendanceRecordFileName(courseId, dateStr);
			File f = new File(filePath);
			return f.exists();
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * This method saves the given attendance record object
	 * 
	 * @param ar
	 *            attendance record to save
	 * @return whether the record is saved successfully
	 */
	public boolean saveAttendanceRecord(AttendanceRecord ar) {
		try {
			String filePath = genAttendanceRecordFileName(ar.getCourse()
					.getCourseId(), ar.getDateStr());
			List<String[]> data = new ArrayList<String[]>();

			// Here the given record object is converted to a list of String[],
			// so data manager can write them to file
			for (AttendanceEntryModel entry : ar.getEntries()) {
				String sid = entry.getStudent().getStudentId();
				int status = entry.getAttendanceStatus();
				data.add(new String[] { sid, String.valueOf(status) });
			}
			dataManager.writeCSV(filePath, data);

			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * This method loads attendance record by reading raw record data and
	 * convert that to attendance record object.
	 * 
	 * @param courseId
	 *            ID of a course
	 * @param dateStr
	 *            date string
	 * @param studentsOfCourse
	 *            this is used to populate the student entries in the record
	 *            loaded.
	 * @return the requested attendance record
	 */
	public AttendanceRecord loadAttendanceRecord(Course course, String dateStr,
			List<Student> studentsOfCourse) {
		try {
			String filePath = genAttendanceRecordFileName(course.getCourseId(),
					dateStr);
			List<String[]> data = dataManager.readCSV(filePath);

			List<AttendanceEntryModel> entries = new ArrayList<AttendanceEntryModel>();
			for (String[] row : data)
				for (Student student : studentsOfCourse) {
					if (student.getStudentId().equals(row[0])) {
						AttendanceEntryModel am = new AttendanceEntryModel(
								student, Integer.valueOf(row[1]));
						entries.add(am);
					}
				}

			AttendanceRecord result = new AttendanceRecord(course, dateStr,
					entries);

			return result;
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * This method deletes an attendance record based on the given courseID and
	 * date string
	 * 
	 * @param courseId
	 *            ID of a course
	 * @param dateStr
	 *            date string
	 * @return whether the attendance record is deleted successfully
	 */
	public boolean deleteAttendanceRecord(String courseId, String dateStr) {
		try {
			String filePath = genAttendanceRecordFileName(courseId, dateStr);
			File f = new File(filePath);
			if (f.exists()) {
				f.delete();
				return true;
			}

			return false;
		} catch (Exception e) {
			return false;
		}
	}
}
