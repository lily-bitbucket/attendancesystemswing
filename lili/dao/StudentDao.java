package lili.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import lili.Config;
import lili.domainmodel.Student;
import lili.service.io.DataManager;

/**
 * This class is the data access object for students. It can save, read and
 * delete student data.
 * 
 * @author Shuangli Wu
 * @NetID sxw133730
 * @date 03/25/2014
 * @purpose CS6359-002 Assignment4
 */
public class StudentDao extends Observable {

	/** A DataManager object is used to access persisted data. */
	public DataManager dataManager = DataManager.getUniqueInstance();

	/**
	 * Add a student to student data file
	 * 
	 * @param studentId
	 *            ID of the student to add
	 * @param firstName
	 *            first name of the student to add
	 * @param lastName
	 *            last name of the student to add
	 * @param imagePath
	 *            path of the image of the student to add
	 * @return whether adding the student is successful
	 */
	public boolean addStudent(String studentId, String firstName,
			String lastName, String imagePath) {
		try {
			dataManager.appendToCSV(Config.STUDENT_CSV, new String[] {
					studentId, firstName, lastName, imagePath });
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Load student data form the student data file and populate a list with
	 * Student objects
	 * 
	 * @return a list of student data
	 */
	public List<Student> loadStudents() {
		List<Student> students = new ArrayList<Student>();
		try {
			List<String[]> data = dataManager.readCSV(Config.STUDENT_CSV);

			for (String[] row : data) {
				Student student = null;
				if (row.length == 4)
					student = new Student(row[0], row[1], row[2], row[3]);
				else
					student = new Student(row[0], row[1], row[2], "");

				students.add(student);
			}

			return students;
		} catch (Exception e) {
			return students;
		}
	}

	/**
	 * Load student data form the student data file, do not convert to Student
	 * objects
	 * 
	 * @return a 2D string array of raw student data
	 */
	public String[][] loadStudentsData() {
		try {
			return dataManager.readCSV2DArray(Config.STUDENT_CSV);
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * Load student data form the student data file, do not convert to Student
	 * objects
	 * 
	 * @return a list of string array of raw student data
	 */
	public List<String[]> loadStudentsDataList() {
		try {
			return dataManager.readCSV(Config.STUDENT_CSV);
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * Save raw stsudent data to student data file
	 * @param data given raw student data
	 * @return whether data has been successfully saved
	 */
	public boolean saveStudentsData(List<String[]> data) {
		try {
			// This must be called before changing data file
			setChanged();
			notifyObservers(data);
			dataManager.writeCSV(Config.STUDENT_CSV, data);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Delete a student's data from the student data file by student ID
	 * @param studentId given studentID
	 * @return whether student's data has been successfully saved
	 */
	public boolean deleteStudentById(String studentId) {
		try {
			List<String[]> list = loadStudentsDataList();
			for (int i = 0; i < list.size(); i++) {
				String[] rowData = list.get(i);
				if (rowData[0].equals(studentId))
					list.remove(i);
			}
			setChanged();
			notifyObservers(studentId);
			saveStudentsData(list);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
}
